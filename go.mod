module airqi-be

go 1.16

require (
	github.com/SebastiaanKlippert/go-wkhtmltopdf v1.7.2
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/logger v0.0.2
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang-migrate/migrate/v4 v4.15.1
	github.com/gorilla/websocket v1.4.2
	github.com/joho/godotenv v1.4.0
	github.com/mo-taufiq/go-logger v1.1.1
	github.com/rs/zerolog v1.26.1
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/cast v1.3.1
	github.com/spf13/cobra v1.4.0
	github.com/spf13/viper v1.8.1
	github.com/stoewer/go-strcase v1.2.0
	github.com/streadway/amqp v1.0.0
	golang.org/x/crypto v0.0.0-20211215165025-cf75a172585e
	gorm.io/driver/mysql v1.3.2
	gorm.io/driver/sqlserver v1.3.1
	gorm.io/gorm v1.23.3
)
