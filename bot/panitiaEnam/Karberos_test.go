package panitiaEnam

import (
	"airqi-be/internal/app/common/httpRequest"
	"airqi-be/internal/app/model/sensorModel"
	"encoding/json"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
	uuid "github.com/satori/go.uuid"
	"math/rand"
	"os"
	"testing"
	"time"
)

func TestNormalCaseKerberos(test *testing.T) {

	err := godotenv.Load("../../params/.env")
	if err != nil {
		log.Error().Msg("Error loading .env file")
	}

	host := "http://localhost"
	port := os.Getenv("APP_PORT")

	url := host + ":" + port + "/api/v1/sensors/insert/sensor-read"

	apSensorID := Karberos

	feedInterval := time.Second

	ticker := time.NewTicker(feedInterval)

	min := KarberosMin
	max := KarberosMax

	testCount := 0
	testMax := 99999999

	for {
		select {
		case _ = <-ticker.C:
			testCount++

			if testCount > testMax {
				test.Log("test finished")
				return
			}

			rand.Seed(time.Now().UnixNano())
			readValue := rand.Intn(max-min+1) + min

			reqBody := sensorModel.SensorInsertReq{
				ApSensorId:       apSensorID,
				ReadValue:        int64(readValue),
				ReadDesc:         "test read value",
				ExtendInfo:       "",
				SeqNo:            0,
				ReadUID:          uuid.NewV4().String(),
				OriginGMTCreated: time.Now().Unix(),
			}

			reqBodyByte, err := json.Marshal(&reqBody)
			if err != nil {
				test.Error(err.Error())
				return
			}

			code, _, err := httpRequest.PostData(url, reqBodyByte, httpRequest.HttpHeaders{
				"Content-Type": "application/json",
			}, 12)
			if err != nil {
				test.Log(err.Error())
				continue
			}

			test.Log(code)
		}
	}

}
