package panitiaEnam

const (
	Karberos    = "APSENSORCO2GNID001"
	KarberosMin = 430
	KarberosMax = 440

	Serpent    = "APSENSORTMPGNID001"
	SerpentMin = 21
	SerpentMax = 22

	Leviathan    = "APSENSORHMDGNID001"
	LeviathanMin = 40
	LeviathanMax = 45
)
