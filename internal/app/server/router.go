package server

import (
	"airqi-be/internal/app/handler"
	"github.com/rs/zerolog/log"
	"os"
	"strings"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
)

func Router(opt handler.HandlerOption) *gin.Engine {
	// the handler
	utilHandler := handler.NewUtilDelivery()
	authHandler := handler.AuthHandler{HandlerOption: opt}
	sensorHandler := handler.SensorHandler{HandlerOption: opt}
	notifHandler := handler.NotifHandler{HandlerOption: opt}

	setMode := os.Getenv("SET_MODE")
	if strings.ToLower(setMode) == "production" {
		gin.SetMode(gin.ReleaseMode)
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	} else {
		gin.SetMode(gin.DebugMode)
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
	//routes
	r := gin.New()
	r.Use(cors.New(cors.Config{
		AllowAllOrigins:        true,
		AllowMethods:           []string{"POST", "DELETE", "GET", "OPTIONS", "PUT"},
		AllowHeaders:           []string{"Origin", "Content-Type", "Authorization", "userid", "REQUEST-ID", "X-SIGNATURE", "Referer", "User-Agent"},
		AllowCredentials:       true,
		ExposeHeaders:          []string{"Content-Length"},
		MaxAge:                 120 * time.Second,
		AllowWildcard:          true,
		AllowBrowserExtensions: true,
		AllowWebSockets:        true,
		AllowFiles:             true,
	}))

	r.Use(gin.Recovery())
	r.GET("/healthy-check", utilHandler.HealthyCheck)

	//Maximum memory limit for Multipart forms
	r.MaxMultipartMemory = 8 << 20 // 8 MiB

	{
		authAPIGroupUnsecured := r.Group("/api/v1/auth")
		{
			authAPIGroupUnsecured.POST("/login", authHandler.Login)
		}

		//apiGroupAuthorized := r.Group("/api/v1/auth",
		//	opt.AuthMiddleware.AuthorizeJWTWithUserContext(roleConstants.AllRole()))
		//{
		//	apiGroupAuthorized.POST("/change-password", authHandler.ChangePassword)
		//}
	}

	/*		User Route
	 */
	//userAPIGroup := r.Group("/api/v1/user",
	//	opt.AuthMiddleware.AuthorizeJWTWithUserContextV2(roleConstants.AdministratorRole))
	//{
	//	userAPIGroup.POST("/create", userHandler.AddNewUser)
	//	userAPIGroup.GET("/all", userHandler.GetAllUser)
	//	userAPIGroup.PUT("/update", userHandler.UpdateUser)
	//	userAPIGroup.GET("/role/all", userHandler.GetAllUserRole)
	//}

	/*
		Sensor Route
	*/
	sensorAPIGroup := r.Group("/api/v1/sensors")
	{
		sensorAPIGroup.GET("/data/all-sensor", sensorHandler.GetAllSensor)
		sensorAPIGroup.GET("/data/sensor-read", sensorHandler.GetSensorReadBySensorID)
		sensorAPIGroup.GET("/data/sensor-reads-average", sensorHandler.GetAverageSensorReadsBySensorID)
		sensorAPIGroup.POST("/data/sensor/update-info", sensorHandler.UpdateSensorInfo)
		sensorAPIGroup.GET("/data/sensor/last-average", sensorHandler.GetAverageSensorReadBySensorID)
		//sensorAPIGroup.POST("/data/csv", sensorHandler.GetSensorReadInCSVData)
		sensorAPIGroup.POST("/insert/sensor-read", sensorHandler.InsertSensorRead)
	}

	notifAlertAPIGroup := r.Group("/api/v1/notifications")
	{
		notifAlertAPIGroup.GET("/alerts", notifHandler.GetAllNotifAlert)
	}

	/*
		Sensor Listener Handler
	*/
	{
		go func() {
			err := sensorHandler.InitSensorListeners()
			if err != nil {
				log.Error().Msg(err.Error())
			}
		}()
	}

	/*
		WS Routers
	*/
	handler.NewSensorWSHandler(opt.WSHandler, opt.SensorReadListenerService, opt.NotifAlertListenerService)

	return r
}
