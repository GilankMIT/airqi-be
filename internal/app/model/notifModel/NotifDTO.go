package notifModel

type SendNotifAlertReq struct {
	Severity    ApNotifAlertSeverity
	Title       string            `json:"title"`
	SensorID    int64             `json:"sensor_id"`
	ThresholdID int64             `json:"threshold_id"`
	Content     string            `json:"content"`
	ExtendInfo  map[string]string `json:"extend_info"`
	AlertUID    string            `json:"alert_uid"`
}

type SendNotifAlertProcessorReq struct {
	SendNotifAlertReq
	Account ApNotifAccount `json:"account"`
}

type SendNotifAlertProcessorRes struct {
	IsSuccess bool `json:"is_success"`
	IsSkipped bool `json:"is_skipped"`
}
