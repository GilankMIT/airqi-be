package notifModel

import "airqi-be/internal/app/model/helperModel"

/* Data Type Definition */
/* ap_notif_alert_status data type */
type ApNotifAlertStatus int8

/* ap_notif_alert_severity_data type */
type ApNotifAlertSeverity int8

/* Table Definition */

/* ap_notif_alerts table */
type ApNotifAlert struct {
	ID               int64                `json:"id"`
	ApAlertID        string               `json:"ap_alert_id"`
	Content          string               `json:"content"`
	ApNotifAccountID int64                `json:"ap_notif_account_id"`
	Title            string               `json:"title"`
	Status           ApNotifAlertStatus   `json:"status"`
	Severity         ApNotifAlertSeverity `json:"severity"`
	SensorID         int64                `json:"sensor_id"`
	ThresholdID      int64                `json:"threshold_id"`
	ExtendInfo       string               `json:"extend_info"`
	helperModel.DateAuditModel
	helperModel.UserAuditModel
}

/* ap_notif_accounts table */
type ApNotifAccount struct {
	ID           int64  `json:"id"`
	NotifType    string `json:"notif_type"`
	Token        string `json:"token"`
	IsTokenValid bool   `json:"is_token_valid"`
	IsMainAcc    bool   `json:"is_main_acc"`
	IsEnable     bool   `json:"is_enable"`
	ShouldRetry  bool   `json:"should_retry"`
	MaxRetry     int    `json:"max_retry"`
	Priority     int    `json:"priority"`
	URL          string `json:"url"`
	Method       string `json:"method"`
	helperModel.DateAuditModel
	helperModel.UserAuditModel
}
