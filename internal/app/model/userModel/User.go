package userModel

import "airqi-be/internal/app/model/helperModel"

/* Table Definition */

/* ap_user table */
type ApUser struct {
	ID        int64  `json:"id"`
	UID       string `json:"uid"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	helperModel.DateAuditModel
	helperModel.UserAuditModel
}

/* ap_role table */
type ApRole struct {
	ID                   int64              `json:"id"`
	ApUserID             int64              `json:"ap_user_id"`
	User                 ApUser             `json:"user" gorm:"-"`
	APActionPermissionID int                `json:"ap_action_permission_id"`
	APActionPermission   ApActionPermission `json:"ap_action_permission"`
	helperModel.DateAuditModel
	helperModel.UserAuditModel
}

/* ap_action_permission table */
type ApActionPermission struct {
	ID     int64  `json:"id"`
	URI    string `json:"uri"`
	Method string `json:"method"`
	helperModel.DateAuditModel
	helperModel.UserAuditModel
}
