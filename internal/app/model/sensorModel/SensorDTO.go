package sensorModel

/* DTO Definition */

type SensorInsertReq struct {
	ApSensorId       string `json:"ap_sensor_id" binding:"required"`
	ReadValue        int64  `json:"read_value"`
	ReadDesc         string `json:"read_desc"`
	ExtendInfo       string `json:"extend_info"`
	SeqNo            int64  `json:"seq_no"`
	ReadUID          string `json:"read_uid"`
	OriginGMTCreated int64  `json:"origin_gmt_created"`
}

type ReqUpdateSensorInfo struct {
	ApDeviceID              string `json:"ap_device_id"`
	SensorName              string `json:"sensor_name"`
	ApSensorType            string `json:"ap_sensor_type"`
	SensorIP                string `json:"sensor_ip"`
	IsVisible               bool   `json:"is_visible"`
	IsEnable                bool   `json:"is_enable"`
	PassThresholdTimespanMS int64  `json:"pass_threshold_timespan_ms"`
	AlertContent            string `json:"alert_content"`
}

type SensorInsertRes struct {
	Desc string `json:"desc"`
}

type SensorReadMinifyRes struct {
	ApSensorID       string `json:"ap_sensor_id"`
	ReadValue        int64  `json:"read_value"`
	ReadDesc         string `json:"read_desc"`
	ReadUID          string `json:"read_uid"`
	OriginGMTCreated int64  `json:"origin_gmt_created"`
}

type ReqSensorReadInfo struct {
	ApDeviceID string `json:"ap_device_id"`
	TimeMin    int64  `json:"time_min"`
	TimeMax    int64  `json:"time_max"`
}

type SensorReadInfo struct {
	ReadValue     int64  `json:"read_value"`
	Timestamp     int64  `json:"timestamp"`
	TimestampText string `json:"timestamp_text"`
}

type ResSensorInfo struct {
	SensorReadInfoList []SensorReadInfo `json:"sensor_read_info_list"`
}

type ResSensorAverage struct {
	ReadValue     int64  `json:"read_value"`
	ThresholdName string `json:"threshold_name"`
}

type ReqSensorReadWS struct {
	SensorID string `json:"sensor_id"`
}

type ResSensorReadWS struct {
	ReadValue        int64  `json:"read_value"`
	ThresholdName    string `json:"threshold_name"`
	AverageReadValue int64  `json:"average_read_value"`
	Timestamp        string `json:"timestamp"`
	Time             string `json:"time"`
}
