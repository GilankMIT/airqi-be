package sensorModel

import "airqi-be/internal/app/model/helperModel"

/* Table Definition */

/* ap_sensors table */
type ApSensor struct {
	ID                      int64  `json:"id"`
	ApDeviceID              string `json:"ap_device_id"`
	SensorName              string `json:"sensor_name"`
	ApSensorType            string `json:"ap_sensor_type"`
	SensorIP                string `json:"sensor_ip"`
	IsVisible               bool   `json:"is_visible"`
	IsEnable                bool   `json:"is_enable"`
	PassThresholdTimespanMS int64  `json:"pass_threshold_timespan_ms"`
	AlertContent            string `json:"alert_content"`
	helperModel.DateAuditModel
	helperModel.UserAuditModel
}

/* ap_sensor_types table */
type ApSensorType struct {
	ID          int    `json:"id"`
	Type        string `json:"type"`
	IsTelemetry int    `json:"is_telemetry"`
	helperModel.DateAuditModel
	helperModel.UserAuditModel
}

/* ap_sensor_thresholds table */
type ApSensorThreshold struct {
	ID                          int64  `json:"id"`
	ApSensorId                  int    `json:"ap_sensor_id"`
	TopThresholdValue           int64  `json:"top_threshold_value"`
	BottomThresholdValue        int64  `json:"bottom_threshold_value"`
	PassThresholdTimespanSecond int64  `json:"pass_threshold_timespan_second"`
	IsEnable                    bool   `json:"is_enable"`
	SendNotif                   bool   `json:"send_notif"`
	Desc                        string `json:"desc"`
	Severity                    int    `json:"severity"`
	helperModel.UserAuditModel
	helperModel.DateAuditModel
}

/* ap_sensor_reads table */
type ApSensorRead struct {
	ID               int64  `json:"id"`
	ApSensorId       int64  `json:"ap_sensor_id"`
	ApDeviceID       string `json:"ap_device_id"`
	ReadValue        int64  `json:"read_value"`
	ReadDesc         string `json:"read_desc"`
	ExtendInfo       string `json:"extend_info"`
	SeqNo            int64  `json:"seq_no"`
	ReadUID          string `json:"read_uid"`
	OriginGMTCreated int64  `json:"origin_gmt_created"`
	helperModel.UserAuditModel
	helperModel.DateAuditModel
}

/* ap_pass_threshold_histories table */
type ApPassThresholdHistory struct {
	ID                  int64 `json:"id"`
	ApSensorId          int64 `json:"ap_sensor_id"`
	ApSensorThresholdID int64 `json:"ap_sensor_threshold_id"`
	LastReadValue       int64 `json:"last_read_value"`
	NotifSent           bool  `json:"notif_sent"`
	OriginGMTCreated    int64 `json:"origin_gmt_created"`
	helperModel.UserAuditModel
	helperModel.DateAuditModel
}
