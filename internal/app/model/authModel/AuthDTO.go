package authModel

import "airqi-be/internal/app/model/userModel"

type ReqAuthenticate struct {
	LoginScenario int    `json:"login_scenario"`
	Username      string `json:"username"`
	Credential    string `json:"credential"`
}

type ResAuthenticate struct {
	AuthToken string           `json:"auth_token"`
	User      userModel.ApUser `json:"user"`
}
