package helperModel

type DateAuditModel struct {
	GMTCreated  int64 `json:"gmt_create"`
	GMTModified int64 `json:"gmt_modified"`
}
