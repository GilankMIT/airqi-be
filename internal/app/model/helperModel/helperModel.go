package helperModel

import "net/http"

const (
	AdministratorRole = "administrator"
)

var UserRoleID = map[string]int{
	AdministratorRole: 1,
}

var RoleName = map[int]string{
	UserRoleID[AdministratorRole]: AdministratorRole,
}

//AllRole return string for all role route compatible
func AllRole() string {
	allRole := ""
	for _, userRole := range RoleName {
		if allRole != "" {
			allRole += "|"
		}
		allRole += userRole
	}

	return allRole
}

//mapping error validation
const (
	RequiredMsg = "is required"
	MaxMsg      = "maximum : "
	MinMsg      = "minimum : "
	NoChange    = "no change"

	OTP_Type_Email = "email"
	OTP_Type_SMS   = "sms"

	OTP_STATUS_VERIFIED = "otp_verified"
	OTP_STATUS_USED     = "otp_used"
)

// SLIK Helper
const (
	SLIK_RECORD_EXIST    = "SLIK RECORD EXIST"
	SLIK_RECORD_NOTEXIST = "NO SLIK RECORD"
)

const (
	LanguageID = "ID"
)

type ResponseMetadata struct {
	Message      string `json:"message"`
	ResponseCode string `json:"response_code"`
	HTTPStatus   int    `json:"http_status"`
}

type ErrorMessage struct {
	Url      string `json:"url"`
	Request  string `json:"request"`
	Response string `json:"response"`
	Message  string `json:"message"`
}

func DefaultResponseMetadata() ResponseMetadata {
	return ResponseMetadata{
		Message:      "success",
		ResponseCode: "200",
		HTTPStatus:   http.StatusOK,
	}
}
