package helperModel

type UserAuditModel struct {
	CreatedBy int64 `json:"created_by"`
	UpdatedBy int64 `json:"updated_by"`
}
