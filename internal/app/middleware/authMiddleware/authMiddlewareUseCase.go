package authMiddleware

import (
	"airqi-be/internal/app/common/jsonHttpResponse"
	"airqi-be/internal/app/common/jwtHelper"
	"airqi-be/internal/app/model/userModel"
	"airqi-be/internal/app/repository/userRepository"
	"errors"
	"github.com/rs/zerolog/log"
	"strings"

	"github.com/gin-gonic/gin"
)

var (
	ErrInvalidToken       = errors.New("invalid token")
	ErrUserNotFound       = errors.New("user not found")
	ErrURLNotFound        = errors.New("url token not found")
	ErrorContextNotExist  = errors.New("user context not exist")
	ErrorParsingUserModel = errors.New("error parsing user model")
)

const (
	ApUserRouteContextName = "ap_user"
)

type authMiddleware struct {
	userRepo userRepository.IUserRepository
}

func NewAuthMiddleware(userRepo userRepository.IUserRepository) IAuthMiddleware {
	return &authMiddleware{userRepo: userRepo}
}

//AuthorizeJWTWithUserContext - Authorize JWT with User Context (Need to look up for user in DB in every request)
func (auth *authMiddleware) AuthorizeJWTWithUserContext() gin.HandlerFunc {
	return func(c *gin.Context) {
		bearerToken := c.GetHeader("Authorization")
		userData, err := auth.getUserFromJWT(bearerToken)
		if err != nil {
			if err == ErrInvalidToken ||
				err == ErrUserNotFound {
				res := jsonHttpResponse.FailedResponse{
					Status:       jsonHttpResponse.FailedStatus,
					ResponseCode: "00",
					Message:      err.Error(),
				}
				jsonHttpResponse.Unauthorized(c, res)
				c.Abort()
				return
			}

			res := jsonHttpResponse.FailedResponse{
				Status:       jsonHttpResponse.FailedStatus,
				ResponseCode: "00",
				Message:      err.Error(),
			}
			jsonHttpResponse.InternalServerError(c, res)
			c.Abort()
			return
		}

		c.Set("user", &userData)
		c.Next()
		return
	}
}

//AuthorizeURLAction - Authorize JWT with User Context (Need to look up for user in DB in every request)
func (auth *authMiddleware) AuthorizeURLAction() gin.HandlerFunc {
	return func(c *gin.Context) {

		apUserData, err := GetUserFromJWTContext(c)
		if err != nil {
			log.Info().Msg("authMiddleware : AuthorizeURLAction error " + err.Error())
			jsonHttpResponse.NewFailedUnauthorizedResponse(c, err.Error())
			c.Abort()
			return
		}

		//check route permission for user
		/* TODO : retrieve from cache control */
		apRoles, err := auth.userRepo.GetUserRoleByUserID(apUserData.ID)
		if err != nil {
			log.Info().Msg("authMiddleware : AuthorizeURLAction error " + err.Error())
			jsonHttpResponse.NewFailedUnauthorizedResponse(c, err.Error())
			c.Abort()
			return
		}

		for _, apRole := range *apRoles {
			if apRole.APActionPermission.URI == c.FullPath() {
				c.Next()
				return
			}
		}

		log.Info().Msg("authMiddleware : AuthorizeURLAction no match url")
		jsonHttpResponse.NewFailedUnauthorizedResponse(c, "you don't have permission")
		c.Abort()
		return
	}
}

func (auth *authMiddleware) getUserFromJWT(jwtToken string) (user userModel.ApUser, err error) {
	if jwtToken == "" {
		return user, ErrInvalidToken
	}

	//Extract JWT Token from Bearer
	jwtTokenSplit := strings.Split(jwtToken, "Bearer ")
	if jwtTokenSplit[1] == "" {
		return user, ErrInvalidToken
	}
	tokenSegment := jwtTokenSplit[1]

	jwtTokenClaims, err := jwtHelper.VerifyTokenWithAPUserClaim(tokenSegment)
	if err != nil {
		return user, ErrInvalidToken
	}

	apUserData, err := auth.userRepo.GetUserByID(jwtTokenClaims.APUserID)
	if err != nil {
		return user, err
	}

	return *apUserData, ErrInvalidToken
}

func GetUserFromJWTContext(c *gin.Context) (*userModel.ApUser, error) {
	user, exists := c.Get(ApUserRouteContextName)
	if !exists {
		return nil, ErrorContextNotExist
	}

	if user == "" {
		return nil, ErrorContextNotExist
	}

	userData, ok := user.(*userModel.ApUser)
	if !ok {
		return nil, ErrorParsingUserModel
	}

	return userData, nil
}
