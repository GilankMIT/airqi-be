package authMiddleware

import "github.com/gin-gonic/gin"

type IAuthMiddleware interface {
	AuthorizeJWTWithUserContext() gin.HandlerFunc
}
