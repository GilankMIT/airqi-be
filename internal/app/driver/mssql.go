package driver

import (
	"database/sql"
	"fmt"
	"gorm.io/driver/sqlserver"
	_ "gorm.io/driver/sqlserver"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"os"
	"time"
)

// DBMysqlOption options for mysql connection
type DBMssqlOption struct {
	IsEnable             bool
	Host                 string
	Port                 int
	Username             string
	Password             string
	DBName               string
	AdditionalParameters string
	MaxOpenConns         int
	MaxIdleConns         int
	ConnMaxLifetime      time.Duration
}

// NewMssqlDatabase return gorp dbmap object with MsSQL options param
func NewMssqlDatabase(option DBMssqlOption) (*gorm.DB, error) {
	dbDsn := fmt.Sprintf("sqlserver://%s:%s@%s:%d?database=%s", option.Username, option.Password, option.Host, option.Port, option.DBName)
	fmt.Println(dbDsn)
	db, err := sql.Open("mssql", dbDsn)
	if err != nil {
		fmt.Println(dbDsn)
		return nil, err
	}

	db.SetConnMaxLifetime(option.ConnMaxLifetime)
	db.SetMaxIdleConns(option.MaxIdleConns)
	db.SetMaxOpenConns(option.MaxOpenConns)

	gormDB, err := gorm.Open(sqlserver.New(sqlserver.Config{
		Conn: db,
	}), &gorm.Config{})
	if err != nil {
		return nil, err
	}
	setMode := os.Getenv("SET_MODE")
	if setMode == "production" {
		gormDB.Config.Logger = logger.Default.LogMode(logger.Error)
	} else {
		gormDB.Config.Logger = logger.Default.LogMode(logger.Info)
	}

	return gormDB, nil
}
