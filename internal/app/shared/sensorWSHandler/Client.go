package server

import (
	"airqi-be/internal/app/handler"
	"github.com/gorilla/websocket"
	"github.com/satori/go.uuid"
)

type Client struct {
	Hub              *handler.Hub
	Conn             *websocket.Conn
	Send             chan []byte
	ConnectionID     uuid.UUID
	userID           int
	SessionKey       string
	ShouldDisconnect bool
}
