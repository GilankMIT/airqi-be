package websocket

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/logger"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"os"
	"regexp"
	"time"
)

type WebsocketHandler struct {
	r       *gin.Engine
	address string
}

//NewWebsocketHandler will return new handler for websocket
func NewWebsocketHandler(address string) *WebsocketHandler {

	//routes
	r := gin.New()

	var rxURL = regexp.MustCompile(`^/regexp\d*`)
	// Add a logger middleware, which:
	//   - Logs all requests, like a combined access and error log.
	//   - Logs to stdout.=
	subLog := zerolog.New(os.Stdout).With().Logger()

	r.Use(logger.SetLogger(logger.Config{
		Logger:         &subLog,
		UTC:            true,
		SkipPath:       []string{"/skip"},
		SkipPathRegexp: rxURL,
	}))

	r.Use(gin.Recovery())

	r.Use(cors.New(cors.Config{
		AllowAllOrigins:        true,
		AllowMethods:           []string{"POST", "DELETE", "GET", "OPTIONS", "PUT"},
		AllowHeaders:           []string{"Origin", "Content-Type", "Authorization", "userid", "REQUEST-ID", "X-SIGNATURE", "Referer", "User-Agent"},
		AllowCredentials:       true,
		ExposeHeaders:          []string{"Content-Length"},
		MaxAge:                 120 * time.Second,
		AllowWildcard:          true,
		AllowBrowserExtensions: true,
		AllowWebSockets:        true,
		AllowFiles:             true,
	}))

	return &WebsocketHandler{r: r, address: address}
}

//GetWSRouter will return pointer of WebsocketHandler struct gin router
func (websocket *WebsocketHandler) GetWSRouter() *gin.Engine {
	return websocket.r
}

//RunWebsocketServer will run gin router as websocket server handler
func (websocket *WebsocketHandler) RunWebsocketServer() error {
	err := websocket.r.Run(websocket.address)
	//err := websocket.r.RunTLS(websocket.address, "params/ssl_cert/localhost.crt", "params/ssl_cert/localhost.key")
	if err != nil {
		return err
	}
	return nil
}
