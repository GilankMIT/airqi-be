package service

import (
	commons "airqi-be/internal/app/common"
	"airqi-be/internal/app/repository"
	"airqi-be/internal/app/service/authService"
	"airqi-be/internal/app/service/notifService"
	"airqi-be/internal/app/service/sensorService"
)

// Option anything any service object needed
type Option struct {
	commons.Options
	*repository.Repositories
}

type Services struct {
	AuthService                authService.IAuthService
	SensorService              sensorService.ISensorService
	NotifService               notifService.INotifService
	SensorAlertListenerService sensorService.ISensorThresholdActionService
	SensorReadListenerService  sensorService.ISensorReadListenerService
	NotifAlertListenerService  notifService.INotifEventListenerService
}

//plugins
type ServiceProcessor struct {
	NotifServicePlugins []notifService.INotifSendingProcessor
}
