package notifService

import "airqi-be/internal/app/model/notifModel"

type INotifSendingProcessor interface {
	GetOperator() string
	CheckOperand(operator string, res *notifModel.SendNotifAlertProcessorRes) (isProceed bool)
	Send(operator string, req notifModel.SendNotifAlertProcessorReq) (*notifModel.SendNotifAlertProcessorRes, error)
}
