package notifService

import (
	constant2 "airqi-be/internal/app/common/constant"
	"airqi-be/internal/app/common/msgQueue"
	"airqi-be/internal/app/model/helperModel"
	"airqi-be/internal/app/model/notifModel"
	"airqi-be/internal/app/repository/notifRepository"
	"encoding/json"
	"github.com/rs/zerolog/log"
	uuid "github.com/satori/go.uuid"
	"github.com/streadway/amqp"
	"time"
)

type notifService struct {
	notifRepo           notifRepository.INotifRepository
	notifSendProcessors []INotifSendingProcessor
	mqChan              *amqp.Channel
}

func NewNotifService(notifRepo notifRepository.INotifRepository,
	notifSendProcessors []INotifSendingProcessor,
	mqChan *amqp.Channel) INotifService {
	return &notifService{notifRepo: notifRepo,
		notifSendProcessors: notifSendProcessors,
		mqChan:              mqChan}
}

func (n notifService) GetAllNotifAlerts() (*[]notifModel.ApNotifAlert, error) {
	return n.notifRepo.GetAllNotifAlertDesc()
}

func (n notifService) SendNotifAlert(req notifModel.SendNotifAlertReq) (*[]notifModel.ApNotifAlert, error) {
	//find notif account
	notifAcc, err := n.notifRepo.GetAllNotifAccountWhereEnable(true)
	if err != nil {
		log.Error().Msg("failed retrieving notification account " + err.Error())
		return nil, err
	}

	//prepare notif alerts
	apNotifAlerts := make([]notifModel.ApNotifAlert, 0)

	for _, notifAcc := range *notifAcc {
		if n.notifSendProcessors == nil {
			log.Info().Msg("no processor for alerts")
			return &apNotifAlerts, nil
		}

		for _, processor := range n.notifSendProcessors {
			log.Info().Msg("executing process executor " + processor.GetOperator() + " for notifType " + notifAcc.NotifType)
			notifProcReq := notifModel.SendNotifAlertProcessorReq{
				SendNotifAlertReq: notifModel.SendNotifAlertReq{
					Severity:    req.Severity,
					SensorID:    req.SensorID,
					ThresholdID: req.ThresholdID,
					Content:     req.Content,
					ExtendInfo:  req.ExtendInfo,
					AlertUID:    uuid.NewV4().String(),
				},
				Account: notifAcc,
			}

			//execute processors (by plugin)
			res, err := processor.Send(notifAcc.NotifType, notifProcReq)
			if err != nil {
				log.Error().Msg("failed executing notif processor " + processor.GetOperator() +
					" for notifType " + notifAcc.NotifType + ", reason: " + err.Error())
				continue
			}

			if res.IsSkipped {
				log.Info().Msg("processor " + processor.GetOperator() + " skipped for notif type " + notifAcc.NotifType)
			}

			if !res.IsSuccess {
				log.Error().Msg("processor " + processor.GetOperator() + " failed to execute for notif type " + notifAcc.NotifType)
			}

			//TODO : SHOULD BE DONE INSIDE PROCESSOR?
			storedNotifAlert, err := n.storeAlert(notifProcReq.SendNotifAlertReq, notifAcc.ID, notifModel.ApNotifAlertStatus(constant2.NotificationStatusSUCCESS))
			if err != nil {
				log.Error().Msg("SendNotifAlert - cannot store alert data to DB " + err.Error())
				continue
			}

			apNotifAlerts = append(apNotifAlerts, *storedNotifAlert)
		}
	}

	return &apNotifAlerts, nil
}

func (n notifService) storeAlert(alertReq notifModel.SendNotifAlertReq, accountId int64, status notifModel.ApNotifAlertStatus) (*notifModel.ApNotifAlert, error) {
	extendInfoJSON, err := json.Marshal(alertReq.ExtendInfo)
	if err != nil {
		log.Error().Msg("cannot convert extend info to JSON, reason : " + err.Error())
		return nil, err
	}

	alertCurrentTimestamp := time.Now().Unix()

	alertData := notifModel.ApNotifAlert{
		ApAlertID:        alertReq.AlertUID,
		Content:          alertReq.Content,
		ApNotifAccountID: accountId,
		Title:            alertReq.Title,
		Status:           status,
		Severity:         alertReq.Severity,
		SensorID:         alertReq.SensorID,
		ThresholdID:      alertReq.ThresholdID,
		ExtendInfo:       string(extendInfoJSON),
		DateAuditModel: helperModel.DateAuditModel{
			GMTCreated:  alertCurrentTimestamp,
			GMTModified: alertCurrentTimestamp,
		},
	}
	savedNotif, err := n.notifRepo.InsertNotifAlert(alertData)
	if err != nil {
		log.Error().Msg(err.Error())
		return nil, err
	}

	//publish mq service
	bodyJson, err := json.Marshal(alertData)
	if err != nil {
		log.Error().Msg("cannot convert alertData : " + err.Error())
		return nil, err
	}

	err = n.publishMqService(msgQueue.ExchangeNameAlertInfoCommon, bodyJson, "notifModel.ApNotifAlert")
	if err != nil {
		log.Error().Msg("cannot publish alert message : " + err.Error())
		return nil, err
	}

	return savedNotif, nil
}

func (s notifService) publishMqService(exchange string, body []byte, bodyTypeInfo string) error {
	err := s.mqChan.Publish(
		exchange, // exchange
		"",       // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body,
		})

	if err != nil {
		log.Error().Msg("messageQueueService : failed during publishing new message " + err.Error())
		return err
	}

	log.Info().Msg("messageQueueService : success publishing message, with exchange : " + exchange + ", content : " + string(body) +
		", originalBodyType : " + bodyTypeInfo)

	return nil
}
