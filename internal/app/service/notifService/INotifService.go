package notifService

import "airqi-be/internal/app/model/notifModel"

type INotifService interface {
	GetAllNotifAlerts() (*[]notifModel.ApNotifAlert, error)
	SendNotifAlert(req notifModel.SendNotifAlertReq) (*[]notifModel.ApNotifAlert, error)
}

type INotifEventListenerService interface {
	InitListening(sensorUID string, c chan []byte) error
}
