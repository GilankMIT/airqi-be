package notifService

import (
	"airqi-be/internal/app/common/msgQueue"
	"airqi-be/internal/app/repository/notifRepository"
	"airqi-be/internal/app/repository/sensorRepository"
	"errors"
	"github.com/rs/zerolog/log"
	"github.com/streadway/amqp"
)

type notifEventListenerService struct {
	notifRepo  notifRepository.INotifRepository
	sensorRepo sensorRepository.ISensorRepository
	mqChan     *amqp.Channel
}

func NewNotifEventListenerService(notifRepo notifRepository.INotifRepository,
	sensorRepo sensorRepository.ISensorRepository,
	mqChan *amqp.Channel) INotifEventListenerService {
	return &notifEventListenerService{
		notifRepo:  notifRepo,
		sensorRepo: sensorRepo,
		mqChan:     mqChan,
	}
}

func (s notifEventListenerService) InitListening(sensorUID string, c chan []byte) error {
	listenerName := "[" + msgQueue.ExchangeNameAlertInfoCommon + " Listener for New Alert Listener]"

	log.Info().Msg(listenerName + " started")

	queue, err := s.mqChan.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		log.Error().Msg("failed declaring queue" + err.Error())
		return err
	}

	err = s.mqChan.QueueBind(
		queue.Name,                           // queue name
		"",                                   // routing key
		msgQueue.ExchangeNameAlertInfoCommon, // exchange
		false,
		nil,
	)

	if err != nil {
		log.Error().Msg("failed declaring queue bind" + err.Error())
		return err
	}

	//start mq listener for sensor read event (general for now)
	alertEventCommonMsgs, err := s.mqChan.Consume(
		queue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return errors.New("failed initializing consumer for mq, " + err.Error())
	}

	for d := range alertEventCommonMsgs {
		if sensorUID != "*" {
			//TODO
			panic("listening to specific sensor alert is not yet supported")
		}

		log.Info().Msg(listenerName + " mq consumer received " + string(d.Body) + " /[type : " + d.ContentType + "]")

		c <- d.Body
	}

	return errors.New("stop listening " + listenerName)
}
