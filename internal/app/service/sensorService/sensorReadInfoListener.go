package sensorService

import (
	"airqi-be/internal/app/common/constant"
	"airqi-be/internal/app/common/msgQueue"
	"airqi-be/internal/app/common/sensorHelper"
	"airqi-be/internal/app/model/sensorModel"
	"airqi-be/internal/app/repository/sensorRepository"
	"encoding/json"
	"errors"
	"github.com/rs/zerolog/log"
	"github.com/streadway/amqp"
	"strconv"
	"time"
)

type SensorReadListener struct {
	sensorRepo sensorRepository.ISensorRepository
	mqChan     *amqp.Channel
}

func NewSensorReadListener(sensorRepo sensorRepository.ISensorRepository, mqChan *amqp.Channel) ISensorReadListenerService {
	return &SensorReadListener{sensorRepo: sensorRepo, mqChan: mqChan}
}

func (s SensorReadListener) InitListening(sensorUID string, c chan []byte) error {
	listenerName := "[" + msgQueue.ExchangeNameSensorReadCommon + " Listener for Sensor Read Listener]"

	log.Info().Msg(listenerName + " started")

	queue, err := s.mqChan.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		log.Error().Msg("failed declaring queue" + err.Error())
		return err
	}

	err = s.mqChan.QueueBind(
		queue.Name,                            // queue name
		"",                                    // routing key
		msgQueue.ExchangeNameSensorReadCommon, // exchange
		false,
		nil,
	)

	if err != nil {
		log.Error().Msg("failed declaring queue bind" + err.Error())
		return err
	}

	//start mq listener for sensor read event (general for now)
	sensorReadCommonMsgs, err := s.mqChan.Consume(
		queue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return errors.New("failed initializing consumer for mq, " + err.Error())
	}

	for d := range sensorReadCommonMsgs {
		var sensorReadMsg sensorModel.ApSensorRead
		err := json.Unmarshal(d.Body, &sensorReadMsg)
		if err != nil {
			log.Error().Msg("mq consumer failed to parse " + string(d.Body) + " /for_[type : sensorModel.ApSensorRead]")
			continue
		}

		if sensorReadMsg.ApDeviceID != sensorUID {
			continue
		}
		log.Info().Msg(listenerName + " mq consumer received " + string(d.Body) + " /[type : " + d.ContentType + "]")

		//get threshold of sensor
		sensorData, err := s.sensorRepo.GetSensorByDeviceID(sensorReadMsg.ApDeviceID)
		if err != nil {
			log.Error().Msg(listenerName + " failed to retrieve sensor data for read value " +
				strconv.FormatInt(sensorReadMsg.ReadValue, 10))
			continue
		}

		sensorThreshold, err := s.sensorRepo.
			GetSensorThresholdBySensorIDAndReadValueBetweenTopThresholdValueAndBottomThresholdValueAndIsEnable(
				sensorData.ID, sensorReadMsg.ReadValue, true)
		if err != nil {
			log.Error().Msg(listenerName + " failed to retrieve sensor threshold for read value " +
				strconv.FormatInt(sensorReadMsg.ReadValue, 10))
			continue
		}

		//retrieve 30 seconds average
		timeScaleSec := 30
		currentTime := time.Now()
		timeRangeMin := currentTime.Add(-time.Duration(timeScaleSec) * time.Second)
		sensorReads, err := s.sensorRepo.GetSensorReadsFromSensorIDAndReadValueBetweenAndOriginGMTCreatedBetween(sensorData.ID,
			constant.SensorConstant_GlobAccValueMin, constant.SensorConstant_GlobAccValueMax,
			timeRangeMin.Unix(), currentTime.Unix())
		if err != nil {
			log.Error().Msg(listenerName + " failed to retrieve data of sensor reads based on the time scale range " + err.Error())
			continue
		}

		averageReadValue := sensorHelper.GetAverageFromSensorRead(*sensorReads)

		sensorReadRes := sensorModel.ResSensorReadWS{
			ReadValue:        sensorReadMsg.ReadValue,
			ThresholdName:    constant.ThresholdLevelMap[sensorThreshold.Severity],
			AverageReadValue: averageReadValue,
			Timestamp:        currentTime.String()[:19],
			Time:             currentTime.String()[10:19],
		}

		jsonRes, err := json.Marshal(&sensorReadRes)
		if err != nil {
			log.Info().Msg(listenerName + " cannot marshal json res " + err.Error())
			continue
		}

		c <- jsonRes
	}

	return errors.New("stop listening " + listenerName)
}
