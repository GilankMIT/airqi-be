package sensorService

import (
	constant2 "airqi-be/internal/app/common/constant"
	"airqi-be/internal/app/common/sensorHelper"
	"airqi-be/internal/app/common/templateRenderer"
	"airqi-be/internal/app/model/notifModel"
	"airqi-be/internal/app/model/sensorModel"
	"airqi-be/internal/app/repository/sensorRepository"
	"airqi-be/internal/app/service/notifService"
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/streadway/amqp"
	"gorm.io/gorm"
	"strconv"
	"time"
)

type sensorActionService struct {
	sensorRepo   sensorRepository.ISensorRepository
	notifService notifService.INotifService
	mqChan       *amqp.Channel
}

func NewSensorActionService(
	sensorRepo sensorRepository.ISensorRepository,
	notifService notifService.INotifService,
	mqChan *amqp.Channel) ISensorThresholdActionService {
	return &sensorActionService{
		sensorRepo:   sensorRepo,
		notifService: notifService,
		mqChan:       mqChan,
	}
}

func (s sensorActionService) InitListening() error {
	//retrieve all active sensors
	sensorData, err := s.sensorRepo.GetAllSensorWhereIsEnable(true)
	if err != nil {
		return errors.New("cannot start listener, failed getting sensor data " + err.Error())
	}

	forever := make(chan bool)
	for _, sensor := range *sensorData {
		sensorIntmd := sensor
		go func() {
			err = s.ActivateSensor(sensorIntmd)
			if err != nil {
				log.Error().Msg(errors.New("cannot start listener, failed activating sensor " + sensorIntmd.SensorName + " reason: " + err.Error()).Error())
			}
		}()
	}
	<-forever

	return nil
}

func (s sensorActionService) ActivateSensor(sensorData sensorModel.ApSensor) error {
	forever := make(chan bool)

	//listener executors
	//event code : EVCOMM001
	go func() {
		err := s.sensorReadAlertListener(sensorData)
		if err != nil {
			log.Error().Msg(err.Error())
		}
	}()

	//event code : EVCOMM002
	//TODO

	log.Info().Msg(sensorData.SensorName + " " + sensorData.ApDeviceID + " [*] Waiting for messages. To exit press CTRL+C")
	<-forever

	return nil
}

func (s sensorActionService) decideSensorThreshold(sensorRead sensorModel.ApSensorRead) (threshold sensorModel.ApSensorThreshold, err error) {
	//retrieve sensor thresholds
	thresholds, err := s.sensorRepo.GetSensorThresholdsBySensorID(sensorRead.ApSensorId)
	if err != nil {
		return threshold, errors.New("failed retrieving sensor thresholds, " + err.Error())
	}

	for _, thresholdVal := range *thresholds {
		if thresholdVal.BottomThresholdValue <= sensorRead.ReadValue &&
			thresholdVal.TopThresholdValue >= sensorRead.ReadValue {
			return thresholdVal, nil
		}
	}
	return threshold, errors.New("cannot get threshold value for sensor id " +
		strconv.FormatInt(sensorRead.ApSensorId, 10) + " with value " + strconv.FormatInt(sensorRead.ReadValue, 10))
}

func (s sensorActionService) decisionExecutor(
	sensorData sensorModel.ApSensor,
	threshold sensorModel.ApSensorThreshold,
	readValue sensorModel.ApSensorRead) {

	shouldSendAlert := false
	shouldInsertInitialPassThresholdHistory := false

	//get last threshold value for sensorId
	lastThresholdValue, err := s.sensorRepo.GetLastPassThresholdHistoryBySensorID(readValue.ApSensorId)
	if err != nil {
		if gorm.ErrRecordNotFound != err {
			log.Error().Msg("cannot retrieve last pass threshold history and cannot initiate new pass threshold " + err.Error())
			return
		}
		shouldInsertInitialPassThresholdHistory = true
		shouldSendAlert = true
	}

	var averageReadValueThresholdArea sensorModel.ApSensorThreshold

	//decide if notif/alert should be sent
	if !shouldSendAlert {
		//if timespan is not enough for new pass threshold, just return
		if !(time.Now().Unix()-lastThresholdValue.OriginGMTCreated > sensorData.PassThresholdTimespanMS/1000) {
			return
		}

		//if average read value is same with last threshold value
		lastSensorReadTime := time.Now().Add(-time.Second * (time.Duration(sensorData.PassThresholdTimespanMS / 1000)))
		nowTime := time.Now().Unix()

		lastFewReadValues, err := s.sensorRepo.GetSensorReadsFromSensorIDAndReadValueBetweenAndOriginGMTCreatedBetween(sensorData.ID,
			constant2.SensorConstant_GlobAccValueMin, constant2.SensorConstant_GlobAccValueMax, lastSensorReadTime.Unix(), nowTime)
		if err != nil {
			log.Error().Msg("cannot retrieve last sensor reads for sensor id " + strconv.FormatInt(sensorData.ID, 10) + " between GMT timestamp " +
				strconv.FormatInt(lastSensorReadTime.Unix(), 10) + " to " + strconv.FormatInt(nowTime, 10))
			return
		}

		avgReadValue := sensorHelper.GetAverageFromSensorRead(*lastFewReadValues)
		log.Info().Msg("average value for sensor " + sensorData.SensorName + " last " +
			strconv.FormatInt(sensorData.PassThresholdTimespanMS/1000, 10) + " second is " +
			strconv.FormatInt(avgReadValue, 10))

		avgSensorRead := sensorModel.ApSensorRead{
			ApSensorId: sensorData.ID,
			ReadValue:  avgReadValue}
		averageReadValueThresholdArea, err = s.decideSensorThreshold(avgSensorRead)
		if err != nil {
			log.Error().Msg("failed to decide threshold data for average sensor read value " + err.Error())
			return
		}

		if lastThresholdValue.ApSensorThresholdID == averageReadValueThresholdArea.ID {
			return
		}

		shouldSendAlert = true

		//initiate new pass threshold history
		newPassThresholdHistory := sensorModel.ApPassThresholdHistory{
			ApSensorId:          readValue.ApSensorId,
			ApSensorThresholdID: averageReadValueThresholdArea.ID,
			LastReadValue:       readValue.ReadValue,
			NotifSent:           shouldSendAlert,
			OriginGMTCreated:    time.Now().Unix(),
		}

		_, err = s.sensorRepo.InsertSensorPassThresholdHistory(newPassThresholdHistory)
		if err != nil {
			log.Error().Msg("cannot insert new sensor pass threhsold history, reason " + err.Error())
			return
		}
	}

	if shouldInsertInitialPassThresholdHistory {
		//initiate initial pass threshold history
		newPassThresholdHistory := sensorModel.ApPassThresholdHistory{
			ApSensorId:          readValue.ApSensorId,
			ApSensorThresholdID: threshold.ID,
			LastReadValue:       readValue.ReadValue,
			NotifSent:           shouldSendAlert,
			OriginGMTCreated:    time.Now().Unix(),
		}

		_, err = s.sensorRepo.InsertSensorPassThresholdHistory(newPassThresholdHistory)
		if err != nil {
			log.Error().Msg("cannot insert initial sensor pass threhsold history, reason " + err.Error())
			return
		}
	}

	//do plugin framework executions (for future)
	//decide NeedToSendAlert based on anti spam mechanism
	//compose alert
	if shouldSendAlert {
		fmt.Println("SEND ALERT LAHHH")

		if shouldInsertInitialPassThresholdHistory {
			notifAlertReq := s.composeNotifAlert(sensorData, threshold, readValue)
			_, err = s.notifService.SendNotifAlert(notifAlertReq)
			if err != nil {
				log.Error().Msg("failed to send notifications " + err.Error())
			}

			return
		}

		notifAlertReq := s.composeNotifAlert(sensorData, averageReadValueThresholdArea, readValue)
		_, err = s.notifService.SendNotifAlert(notifAlertReq)
		if err != nil {
			log.Error().Msg("failed to send notifications " + err.Error())
		}
	}
}

func (s sensorActionService) composeNotifAlert(sensorData sensorModel.ApSensor,
	threshold sensorModel.ApSensorThreshold,
	readValue sensorModel.ApSensorRead) notifModel.SendNotifAlertReq {

	//re-read sensor data
	latestSensorData, err := s.sensorRepo.GetSensorByDeviceID(sensorData.ApDeviceID)
	if err != nil {
		log.Error().Msg("cannot retrieve sensor data" + err.Error())
		return notifModel.SendNotifAlertReq{}
	}

	sensorData = *latestSensorData

	notifContent := map[string]string{
		"sensor_name":             sensorData.SensorName,
		"device_id":               sensorData.ApDeviceID,
		"sensor_ip":               sensorData.SensorIP,
		"threshold_desc":          threshold.Desc,
		"threshold_severity":      constant2.ThresholdLevelMap[threshold.Severity],
		"threshold_severity_enum": strconv.Itoa(threshold.Severity),
		"read_value":              strconv.FormatInt(readValue.ReadValue, 10),
	}

	return s.buildNotifAlert(sensorData, notifContent)
}

func (s sensorActionService) buildNotifAlert(sensorData sensorModel.ApSensor, contentData map[string]string) notifModel.SendNotifAlertReq {
	//mandatory pass through data
	thresholdSeverityEnum := 0
	thresholdSeverityEnumStr, ok := contentData["threshold_severity_enum"]
	if !ok {
		log.Warn().Msg("cannot retrieve threshold severity data")
	} else {
		thresholdSeverityEnum, _ = strconv.Atoi(thresholdSeverityEnumStr)
	}

	alertContent := templateRenderer.Render(sensorData.AlertContent, contentData)

	return notifModel.SendNotifAlertReq{
		Severity:    notifModel.ApNotifAlertSeverity(thresholdSeverityEnum),
		Title:       "Alert " + sensorData.SensorName,
		SensorID:    sensorData.ID,
		ThresholdID: 0,
		Content:     alertContent,
		ExtendInfo:  contentData,
	}
}
