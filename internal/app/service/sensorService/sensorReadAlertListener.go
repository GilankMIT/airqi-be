package sensorService

import (
	"airqi-be/internal/app/common/msgQueue"
	"airqi-be/internal/app/model/sensorModel"
	"encoding/json"
	"errors"
	"github.com/rs/zerolog/log"
)

func (s sensorActionService) sensorReadAlertListener(sensorData sensorModel.ApSensor) error {
	listenerName := "[" + msgQueue.ExchangeNameSensorReadCommon + " Listener for Sensor Alert Listener]"

	queue, err := s.mqChan.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		log.Error().Msg("failed declaring queue" + err.Error())
		return err
	}

	err = s.mqChan.QueueBind(
		queue.Name,                            // queue name
		"",                                    // routing key
		msgQueue.ExchangeNameSensorReadCommon, // exchange
		false,
		nil,
	)
	if err != nil {
		log.Error().Msg("failed binding queue " + err.Error())
	}

	//start mq listener for sensor read event (general for now)
	sensorReadCommonMsgs, err := s.mqChan.Consume(
		queue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return errors.New("failed initializing consumer for mq, " + err.Error())
	}

	for d := range sensorReadCommonMsgs {
		log.Info().Msg(listenerName + " mq consumer received " + string(d.Body) + " /[type : " + d.ContentType + "]")

		var sensorReadMsg sensorModel.ApSensorRead
		err := json.Unmarshal(d.Body, &sensorReadMsg)
		if err != nil {
			log.Error().Msg("mq consumer failed to parse " + string(d.Body) + " /for_[type : sensorModel.ApSensorRead]")
			continue
		}

		//skip different sensor read
		if sensorReadMsg.ApSensorId != sensorData.ID {
			continue
		}

		threshold, err := s.decideSensorThreshold(sensorReadMsg)
		if err != nil {
			log.Error().Msg("failed to decide threshold data " + err.Error())
			continue
		}

		//pass to decisionExecutor
		s.decisionExecutor(sensorData, threshold, sensorReadMsg)
	}

	return errors.New("databus err - STOPPED : " + listenerName)
}
