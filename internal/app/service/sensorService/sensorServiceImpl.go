package sensorService

import (
	constant2 "airqi-be/internal/app/common/constant"
	"airqi-be/internal/app/common/msgQueue"
	"airqi-be/internal/app/common/sensorHelper"
	"airqi-be/internal/app/model/helperModel"
	"airqi-be/internal/app/model/sensorModel"
	"airqi-be/internal/app/repository/sensorRepository"
	"encoding/json"
	"errors"
	"github.com/rs/zerolog/log"
	"github.com/streadway/amqp"
	"time"
)

type sensorService struct {
	sensorRepo sensorRepository.ISensorRepository
	mqChan     *amqp.Channel
}

func NewSensorService(sensorRepo sensorRepository.ISensorRepository, mqChan *amqp.Channel) ISensorService {
	return &sensorService{sensorRepo: sensorRepo, mqChan: mqChan}
}

func (s sensorService) UpdateSensorData(sensorData sensorModel.ReqUpdateSensorInfo) (*sensorModel.ApSensor, error) {
	existingSensorData, err := s.sensorRepo.GetSensorByDeviceID(sensorData.ApDeviceID)
	if err != nil {
		log.Error().Msg("cannot retrieve sensor data by device id " + sensorData.ApDeviceID + " err reason : " + err.Error())
		return nil, err
	}

	existingSensorData.SensorName = sensorData.SensorName
	existingSensorData.SensorIP = sensorData.SensorIP
	existingSensorData.AlertContent = sensorData.AlertContent

	savedSensorData, err := s.sensorRepo.UpdateSensorData(*existingSensorData)
	if err != nil {
		return nil, err
	}

	return savedSensorData, nil
}

func (s sensorService) AddSensorRead(sensorReadData sensorModel.SensorInsertReq) (*sensorModel.SensorInsertRes, error) {
	//check if sensor target valid
	sensorData, err := s.sensorRepo.GetSensorByDeviceID(sensorReadData.ApSensorId)
	if err != nil {
		log.Error().Msg(err.Error())
		log.Error().Msg("sensor not found " + sensorReadData.ApSensorId)
		return nil, err
	}

	//check if sensor read use duplicate UID
	_, err = s.sensorRepo.GetSensorReadByReadUID(sensorReadData.ReadUID)
	if err == nil {
		err = errors.New("failed to insert new sensor read, read uid already exist / duplicate read uid")
		return nil, err
	}

	//insert new sensor read value
	newSensorReadData := sensorModel.ApSensorRead{
		ApSensorId:       sensorData.ID,
		ApDeviceID:       sensorData.ApDeviceID,
		ReadValue:        sensorReadData.ReadValue,
		ReadDesc:         sensorReadData.ReadDesc,
		ExtendInfo:       sensorReadData.ExtendInfo,
		SeqNo:            sensorReadData.SeqNo,
		ReadUID:          sensorReadData.ReadUID,
		OriginGMTCreated: sensorReadData.OriginGMTCreated,
		UserAuditModel: helperModel.UserAuditModel{
			CreatedBy: sensorData.ID,
		},
		DateAuditModel: helperModel.DateAuditModel{
			GMTCreated: time.Now().Unix(),
		},
	}

	_, err = s.sensorRepo.InsertSensorRead(newSensorReadData)
	if err != nil {
		log.Error().Msg("cannot insert sensor read " + err.Error())
		return nil, err
	}

	//publish mq message
	bodyJson, err := json.Marshal(newSensorReadData)
	if err != nil {
		log.Error().Msg("cannot insert sensor read " + err.Error())
		return nil, err
	}

	//@suppressError
	s.publishMqService(msgQueue.ExchangeNameSensorReadCommon, bodyJson, "sensorModel.ApSensorRead")

	sensorRes := sensorModel.SensorInsertRes{Desc: constant2.SensorConstant_InsertReadValueSucess}

	return &sensorRes, nil
}

func (s sensorService) GetSensorInfoBySensorUID(sensorUID string) (*sensorModel.ApSensor, error) {
	sensorData, err := s.sensorRepo.GetSensorByDeviceID(sensorUID)
	if err != nil {
		log.Error().Msg("cannot get sensor detail for UID " + sensorUID + " reason " + err.Error())
		return nil, err
	}

	return sensorData, nil
}

func (s sensorService) GetAllSensors() (*[]sensorModel.ApSensor, error) {
	return s.sensorRepo.GetAllSensor()
}

func (s sensorService) GetSensorReadInfo(sensorReq sensorModel.ReqSensorReadInfo) (*sensorModel.ResSensorInfo, error) {
	//retrieve sensor data
	sensorData, err := s.sensorRepo.GetSensorByDeviceID(sensorReq.ApDeviceID)
	if err != nil {
		log.Error().Msg("sensor not found " + sensorReq.ApDeviceID + " " + err.Error())
		return nil, err
	}

	sensorReads, err := s.sensorRepo.GetSensorReadsFromSensorIDAndReadValueBetweenAndOriginGMTCreatedBetween(
		sensorData.ID, constant2.SensorConstant_GlobAccValueMin, constant2.SensorConstant_GlobAccValueMax,
		sensorReq.TimeMin, sensorReq.TimeMax)
	if err != nil {
		return nil, err
	}

	sensorInfoList := make([]sensorModel.SensorReadInfo, 0)
	for _, sensorRead := range *sensorReads {
		sensorInfoList = append(sensorInfoList, sensorModel.SensorReadInfo{
			ReadValue: sensorRead.ReadValue,
			Timestamp: sensorRead.OriginGMTCreated,
		})
	}

	sensorInfoRes := sensorModel.ResSensorInfo{
		SensorReadInfoList: sensorInfoList,
	}

	return &sensorInfoRes, nil
}

func (s sensorService) GetAverageSensorReadsInfo(sensorReq sensorModel.ReqSensorReadInfo) (*sensorModel.ResSensorInfo, error) {
	//retrieve sensor data
	sensorData, err := s.sensorRepo.GetSensorByDeviceID(sensorReq.ApDeviceID)
	if err != nil {
		log.Error().Msg("sensor not found " + sensorReq.ApDeviceID + " " + err.Error())
		return nil, err
	}

	sensorReads, err := s.sensorRepo.GetSensorReadsFromSensorIDAndReadValueBetweenAndOriginGMTCreatedBetweenSortBy(
		sensorData.ID, constant2.SensorConstant_GlobAccValueMin, constant2.SensorConstant_GlobAccValueMax,
		sensorReq.TimeMin, sensorReq.TimeMax, "id desc")
	if err != nil {
		return nil, err
	}

	averageTimestep := constant2.AverageTimeStep
	timeRangeMaxTemp := sensorReq.TimeMax
	timeRangeMinTemp := sensorReq.TimeMax - averageTimestep

	tempSensorReads := make([]sensorModel.ApSensorRead, 0)

	averageSensorReads := make([]sensorModel.ApSensorRead, 0)

	sensorInfoList := make([]sensorModel.SensorReadInfo, 0)
	for _, sensorRead := range *sensorReads {

		if sensorRead.GMTCreated <= timeRangeMaxTemp &&
			sensorRead.GMTCreated >= timeRangeMinTemp {
			tempSensorReads = append(tempSensorReads, sensorRead)
		} else { //if found data outside range, then average last tempSensorReads and update time range

			sumSensorReadInRange := int64(0)
			for _, tempSensorReadInRange := range tempSensorReads {
				sumSensorReadInRange += tempSensorReadInRange.ReadValue
			}
			averageSensorReadVal := int64(0)

			if sumSensorReadInRange != 0 {
				averageSensorReadVal = sumSensorReadInRange / int64(len(tempSensorReads))
			}

			//copy information from current sensor read but change some information
			newSensorRead := sensorRead
			newSensorRead.ReadValue = averageSensorReadVal
			newSensorRead.GMTCreated = timeRangeMaxTemp
			averageSensorReads = append(averageSensorReads, newSensorRead)

			//empty temp
			tempSensorReads = make([]sensorModel.ApSensorRead, 0)

			sensorInfoList = append(sensorInfoList, sensorModel.SensorReadInfo{
				ReadValue:     newSensorRead.ReadValue,
				Timestamp:     newSensorRead.GMTCreated,
				TimestampText: time.Unix(newSensorRead.GMTCreated, 0).String()[:19],
			})

			tempSensorReads = append(tempSensorReads, sensorRead)

			//update time range until meet the sensor time range
			for true {
				if sensorRead.GMTCreated <= timeRangeMaxTemp &&
					sensorRead.GMTCreated >= timeRangeMinTemp {
					break
				}
				timeRangeMaxTemp -= averageTimestep
				timeRangeMinTemp -= averageTimestep
			}
		}
	}

	sensorInfoRes := sensorModel.ResSensorInfo{
		SensorReadInfoList: sensorInfoList,
	}

	return &sensorInfoRes, nil
}

func (s sensorService) GetAllSensorReadBySensorID(sensorId string) (*[]sensorModel.ApSensorRead, error) {
	//retrieve sensor data
	sensorData, err := s.sensorRepo.GetSensorByDeviceID(sensorId)
	if err != nil {
		log.Error().Msg(err.Error())
		log.Error().Msg("sensor not found " + sensorId)
		return nil, err
	}

	currentTime := time.Now()
	daysPrior := currentTime.Add(constant2.SensorConstant_GlobTimeRangeMin)

	return s.sensorRepo.GetSensorReadsFromSensorIDAndReadValueBetweenAndOriginGMTCreatedBetween(
		sensorData.ID, constant2.SensorConstant_GlobAccValueMin, constant2.SensorConstant_GlobAccValueMax,
		daysPrior.Unix(), currentTime.Unix())
}

func (s sensorService) GetSensorReadAverageBySensorUID(sensorUID string, timeScaleSec int64) (*sensorModel.ResSensorAverage, error) {
	//retrieve sensor data
	sensorData, err := s.sensorRepo.GetSensorByDeviceID(sensorUID)
	if err != nil {
		log.Error().Msg("sensor not found " + sensorUID + " " + err.Error())
		return nil, err
	}

	currentTime := time.Now()
	timeRangeMin := currentTime.Add(-time.Duration(timeScaleSec) * time.Second)
	sensorReads, err := s.sensorRepo.GetSensorReadsFromSensorIDAndReadValueBetweenAndOriginGMTCreatedBetween(sensorData.ID,
		constant2.SensorConstant_GlobAccValueMin, constant2.SensorConstant_GlobAccValueMax,
		timeRangeMin.Unix(), currentTime.Unix())
	if err != nil {
		log.Error().Msg("failed to retrieve data of sensor reads based on the time scale range " + err.Error())
		return nil, err
	}

	averageReadValue := sensorHelper.GetAverageFromSensorRead(*sensorReads)

	//get threshold of sensor
	sensorThreshold, err := s.sensorRepo.
		GetSensorThresholdBySensorIDAndReadValueBetweenTopThresholdValueAndBottomThresholdValueAndIsEnable(sensorData.ID, averageReadValue, true)
	if err != nil {
		return nil, err
	}

	res := sensorModel.ResSensorAverage{
		ReadValue:     averageReadValue,
		ThresholdName: constant2.ThresholdLevelMap[sensorThreshold.Severity],
	}

	return &res, nil
}

func (s sensorService) publishMqService(exchange string, body []byte, bodyTypeInfo string) error {
	err := s.mqChan.Publish(
		exchange, // exchange
		"",       // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body,
		})

	if err != nil {
		log.Error().Msg("messageQueueService : failed during publishing new message " + err.Error())
		return err
	}

	log.Info().Msg("messageQueueService : success publishing message, with exchange : " + exchange + ", content : " + string(body) +
		", originalBodyType : " + bodyTypeInfo)

	return nil
}
