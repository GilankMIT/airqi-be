package sensorService

import (
	"airqi-be/internal/app/model/sensorModel"
)

type ISensorService interface {
	AddSensorRead(sensorReadData sensorModel.SensorInsertReq) (*sensorModel.SensorInsertRes, error)
	GetAllSensors() (*[]sensorModel.ApSensor, error)
	GetAllSensorReadBySensorID(sensorId string) (*[]sensorModel.ApSensorRead, error)
	UpdateSensorData(sensorData sensorModel.ReqUpdateSensorInfo) (*sensorModel.ApSensor, error)
	GetSensorReadInfo(sensorReq sensorModel.ReqSensorReadInfo) (*sensorModel.ResSensorInfo, error)
	GetAverageSensorReadsInfo(sensorReq sensorModel.ReqSensorReadInfo) (*sensorModel.ResSensorInfo, error)
	GetSensorInfoBySensorUID(sensorUID string) (*sensorModel.ApSensor, error)
	GetSensorReadAverageBySensorUID(sensorUID string, timeScaleSec int64) (*sensorModel.ResSensorAverage, error)
}

type ISensorThresholdActionService interface {
	//sensor pass threshold listener and work service
	InitListening() error
	ActivateSensor(sensorData sensorModel.ApSensor) error
}

type ISensorReadListenerService interface {
	InitListening(sensorUID string, c chan []byte) error
}
