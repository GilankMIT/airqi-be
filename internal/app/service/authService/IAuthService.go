package authService

import "airqi-be/internal/app/model/authModel"

type IAuthService interface {
	AuthenticateUser(authReq authModel.ReqAuthenticate) (authRes *authModel.ResAuthenticate, err error)
}
