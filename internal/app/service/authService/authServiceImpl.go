package authService

import (
	"airqi-be/internal/app/common/applicationConstants"
	"airqi-be/internal/app/common/jwtHelper"
	"airqi-be/internal/app/common/symmetricHash"
	"airqi-be/internal/app/model/authModel"
	"airqi-be/internal/app/repository/userRepository"
	"errors"
	"os"
	"strconv"
	"time"
)

var (
	ErrInvalidCredential = errors.New("invalid credential")
)

type authService struct {
	userRepo userRepository.IUserRepository
}

func NewAuthService(userRepo userRepository.IUserRepository) IAuthService {
	return &authService{
		userRepo: userRepo,
	}
}

func (a authService) AuthenticateUser(authReq authModel.ReqAuthenticate) (authRes *authModel.ResAuthenticate, err error) {
	userData, err := a.userRepo.GetUserByEmail(authReq.Username)
	if err != nil {
		return nil, ErrInvalidCredential
	}

	if !symmetricHash.CompareBcrypt(userData.Password, authReq.Credential) {
		return nil, ErrInvalidCredential
	}

	var authResData authModel.ResAuthenticate
	authResData.User = *userData

	jwtExpirationDurationDayString := os.Getenv(applicationConstants.JWT_EXPIRATION_DURATION_DAY)
	var jwtExpirationDurationDay int
	jwtExpirationDurationDay, err = strconv.Atoi(jwtExpirationDurationDayString)
	if err != nil {
		return authRes, err
	}

	// Conversion to seconds
	jwtExpiredAt := time.Now().Unix() + int64(jwtExpirationDurationDay*3600*24)
	userClaims := jwtHelper.APUserClaim{APUserID: userData.ID, ExpiresAt: jwtExpiredAt, APUserUID: userData.UID}
	jwtToken, err := jwtHelper.NewWithClaims(userClaims)
	if err != nil {
		return nil, err
	}

	authResData.AuthToken = jwtToken

	return &authResData, nil
}
