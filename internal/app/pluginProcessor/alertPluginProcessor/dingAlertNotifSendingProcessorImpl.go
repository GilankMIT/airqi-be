package alertPluginProcessor

import (
	constant2 "airqi-be/internal/app/common/constant"
	"airqi-be/internal/app/common/dingOpenAPI"
	"airqi-be/internal/app/common/httpRequest"
	"airqi-be/internal/app/common/objectDumper"
	"airqi-be/internal/app/model/notifModel"
	"airqi-be/internal/app/repository/sensorRepository"
	"airqi-be/internal/app/service/notifService"
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"os"
	"strings"
)

var DingMandatoryKeyword = ""

type dingAlertNotifSendingProcImpl struct {
	sensorRepo sensorRepository.ISensorRepository
}

func NewDingAlert(sensorRepo sensorRepository.ISensorRepository) notifService.INotifSendingProcessor {
	return &dingAlertNotifSendingProcImpl{sensorRepo: sensorRepo}
}

func (d dingAlertNotifSendingProcImpl) GetOperator() string {
	return "COMMON_ALERT/DING_ALERT_SENDER"
}

func (d dingAlertNotifSendingProcImpl) CheckOperand(operator string, res *notifModel.SendNotifAlertProcessorRes) (isProceed bool) {
	if !strings.Contains(d.GetOperator(), operator) {
		res.IsSuccess = false
		res.IsSkipped = true
		return false
	}

	res.IsSuccess = true
	res.IsSkipped = false

	return true
}

func (d dingAlertNotifSendingProcImpl) Send(operator string, req notifModel.SendNotifAlertProcessorReq) (*notifModel.SendNotifAlertProcessorRes, error) {
	res := notifModel.SendNotifAlertProcessorRes{}

	//generic template execution
	if !d.CheckOperand(operator, &res) {
		return &res, nil
	}

	log.Info().Msg("executing " + d.GetOperator() + " plugin. [Invoke Parameter]: " + objectDumper.DumpStruct(req))

	dingBody := dingOpenAPI.DingBodyMsgText{
		At: struct {
			AtMobiles []string `json:"atMobiles"`
			AtUserIds []string `json:"atUserIds"`
			IsAtAll   bool     `json:"isAtAll"`
		}{
			AtMobiles: nil,
			AtUserIds: nil,
			IsAtAll:   false,
		},
		MsgType: dingOpenAPI.DingMsgTypeTEXT,
		Text: struct {
			Content string `json:"content"`
		}{
			Content: d.buildContent(req.Content),
		},
	}
	dingBodyJson, _ := json.Marshal(dingBody)
	dingUrl := req.Account.URL + "?access_token=" + req.Account.Token

	httpHeaderData := httpRequest.HttpHeaders{
		"Content-Type": "application/json",
	}

	_, _, err := httpRequest.PostData(dingUrl, dingBodyJson, httpHeaderData, 60)
	if err != nil {
		log.Error().Msg(err.Error())
		res.IsSuccess = false
	}

	log.Info().Msg("executed " + d.GetOperator() + " plugin. [Result Parameter]: " + objectDumper.DumpStruct(res))

	return &res, nil
}

func (d dingAlertNotifSendingProcImpl) buildContent(content string) string {
	DingMandatoryKeyword = os.Getenv(constant2.AirqiDingMandatoryKeywordKey)
	fmt.Println(DingMandatoryKeyword, constant2.AirqiDingMandatoryKeywordKey)
	return DingMandatoryKeyword + " " + content
}
