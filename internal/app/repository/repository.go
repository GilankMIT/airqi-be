package repository

import (
	commons "airqi-be/internal/app/common"
	"airqi-be/internal/app/repository/notifRepository"
	"airqi-be/internal/app/repository/sensorRepository"
	"airqi-be/internal/app/repository/userRepository"
)

// Option anything any repo object needed
type Option struct {
	commons.Options
}

type Repositories struct {
	UserRepository   userRepository.IUserRepository
	SensorRepository sensorRepository.ISensorRepository
	NotifRepository  notifRepository.INotifRepository
}
