package notifRepository

import "airqi-be/internal/app/model/notifModel"

type INotifRepository interface {
	GetAllNotifAccountWhereEnable(isEnable bool) (*[]notifModel.ApNotifAccount, error)
	GetAllNotifAlertDesc() (*[]notifModel.ApNotifAlert, error)
	InsertNotifAlert(notifAlert notifModel.ApNotifAlert) (*notifModel.ApNotifAlert, error)
	GetAllNotifAlertWhereStatus(status notifModel.ApNotifAlertStatus) (*[]notifModel.ApNotifAlert, error)
	GetAllNotifAlertWhereStatusAndSensorID(status notifModel.ApNotifAlertStatus,
		sensorId int64) (*[]notifModel.ApNotifAlert, error)
}
