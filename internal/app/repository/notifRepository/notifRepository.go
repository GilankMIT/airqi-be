package notifRepository

import (
	"airqi-be/internal/app/model/notifModel"
	"gorm.io/gorm"
)

type notifRepo struct {
	db *gorm.DB
}

func NewNotifRepo(db *gorm.DB) INotifRepository {
	return &notifRepo{db: db}
}

func (n notifRepo) GetAllNotifAccountWhereEnable(isEnable bool) (*[]notifModel.ApNotifAccount, error) {
	var notifAccs []notifModel.ApNotifAccount
	db := n.db.Find(&notifAccs, "is_enable=?", isEnable)
	return &notifAccs, db.Error
}

func (n notifRepo) GetAllNotifAlertDesc() (*[]notifModel.ApNotifAlert, error) {
	var notifAlerts []notifModel.ApNotifAlert
	db := n.db.Order("id desc").Find(&notifAlerts)
	return &notifAlerts, db.Error
}

func (n notifRepo) InsertNotifAlert(notifAlert notifModel.ApNotifAlert) (*notifModel.ApNotifAlert, error) {
	db := n.db.Create(&notifAlert)
	return &notifAlert, db.Error
}

func (n notifRepo) GetAllNotifAlertWhereStatus(status notifModel.ApNotifAlertStatus) (*[]notifModel.ApNotifAlert, error) {
	var notifAlert []notifModel.ApNotifAlert
	db := n.db.Find(&notifAlert, "status=?", status)
	return &notifAlert, db.Error
}

func (n notifRepo) GetAllNotifAlertWhereStatusAndSensorID(status notifModel.ApNotifAlertStatus,
	sensorId int64) (*[]notifModel.ApNotifAlert, error) {
	var notifAlert []notifModel.ApNotifAlert
	db := n.db.Find(&notifAlert, "status=? AND sensor_id=?", status, sensorId)
	return &notifAlert, db.Error
}
