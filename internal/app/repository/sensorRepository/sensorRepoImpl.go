package sensorRepository

import (
	"airqi-be/internal/app/model/sensorModel"
	"gorm.io/gorm"
)

type sensorRepo struct {
	db *gorm.DB
}

func NewSensorRepository(db *gorm.DB) ISensorRepository {
	return &sensorRepo{db: db}
}

func (s sensorRepo) GetAllSensor() (*[]sensorModel.ApSensor, error) {
	var sensors []sensorModel.ApSensor
	db := s.db.Find(&sensors)
	return &sensors, db.Error
}

func (s sensorRepo) GetSensorByDeviceID(sensorId string) (*sensorModel.ApSensor, error) {
	var sensorData sensorModel.ApSensor
	db := s.db.First(&sensorData, "ap_device_id=?", sensorId)
	return &sensorData, db.Error
}

func (s sensorRepo) GetAllSensorRead() (*[]sensorModel.ApSensorRead, error) {
	var sensorReads []sensorModel.ApSensorRead
	db := s.db.Find(&sensorReads)
	return &sensorReads, db.Error
}

func (s sensorRepo) GetLastSensorReadBySensorUID(sensorUID string) (*sensorModel.ApSensorRead, error) {
	var sensorRead sensorModel.ApSensorRead
	db := s.db.Last(&sensorRead, "ap_device_id = ?", sensorUID)
	return &sensorRead, db.Error
}

func (s sensorRepo) InsertSensorRead(sensorReadData sensorModel.ApSensorRead) (*sensorModel.ApSensorRead, error) {
	db := s.db.Create(&sensorReadData)
	return &sensorReadData, db.Error
}

func (s sensorRepo) GetSensorThresholdsBySensorID(sensorId int64) (*[]sensorModel.ApSensorThreshold, error) {
	var sensorThresholds []sensorModel.ApSensorThreshold
	db := s.db.Find(&sensorThresholds, "ap_sensor_id=?", sensorId)
	return &sensorThresholds, db.Error
}

func (s sensorRepo) GetSensorReadByReadUID(readUID string) (*sensorModel.ApSensorRead, error) {
	var sensorRead sensorModel.ApSensorRead
	db := s.db.First(&sensorRead, "read_uid=?", readUID)
	return &sensorRead, db.Error
}

func (s sensorRepo) GetLastPassThresholdHistoryByThresholdID(thresholdId int64) (*sensorModel.ApPassThresholdHistory, error) {
	var sensorThresholdHistory sensorModel.ApPassThresholdHistory
	db := s.db.Last(&sensorThresholdHistory, "ap_sensor_threshold_id=?", thresholdId)
	return &sensorThresholdHistory, db.Error
}

func (s sensorRepo) GetSensorReadsFromSensorIDAndReadValueBetweenAndOriginGMTCreatedBetween(sensorId int64,
	valueMin, valueMax, timeMin, timeMax int64) (*[]sensorModel.ApSensorRead, error) {
	var sensorReadData []sensorModel.ApSensorRead
	db := s.db.Find(&sensorReadData, "ap_sensor_id=? AND read_value BETWEEN ? AND ? AND gmt_created BETWEEN ? AND ?",
		sensorId, valueMin, valueMax, timeMin, timeMax)
	return &sensorReadData, db.Error
}

func (s sensorRepo) GetSensorReadsFromSensorIDAndReadValueBetweenAndOriginGMTCreatedBetweenSortBy(sensorId int64,
	valueMin, valueMax, timeMin, timeMax int64, sortBy string) (*[]sensorModel.ApSensorRead, error) {
	var sensorReadData []sensorModel.ApSensorRead
	db := s.db.Order(sortBy).Find(&sensorReadData, "ap_sensor_id=? AND read_value BETWEEN ? AND ? AND gmt_created BETWEEN ? AND ?",
		sensorId, valueMin, valueMax, timeMin, timeMax)
	return &sensorReadData, db.Error
}

func (s sensorRepo) GetAllSensorWhereIsEnable(isEnable bool) (*[]sensorModel.ApSensor, error) {
	var sensorData []sensorModel.ApSensor
	db := s.db.Find(&sensorData, "is_enable=?", isEnable)
	return &sensorData, db.Error
}

func (s sensorRepo) GetLastPassThresholdHistoryBySensorID(sensorId int64) (*sensorModel.ApPassThresholdHistory, error) {
	var sensorPassThresholdData sensorModel.ApPassThresholdHistory
	db := s.db.Last(&sensorPassThresholdData, "ap_sensor_id=?", sensorId)
	return &sensorPassThresholdData, db.Error
}

func (s sensorRepo) GetSensorThresholdBySensorIDAndReadValueBetweenTopThresholdValueAndBottomThresholdValueAndIsEnable(sensorId int64,
	readValue int64, isEnable bool) (*sensorModel.ApSensorThreshold, error) {
	var sensorThreshold sensorModel.ApSensorThreshold
	db := s.db.First(&sensorThreshold, "ap_sensor_id=? AND top_threshold_value >= ? AND "+
		"bottom_threshold_value <= ? and is_enable=?", sensorId, readValue, readValue, isEnable)
	return &sensorThreshold, db.Error
}

func (s sensorRepo) InsertSensorPassThresholdHistory(sensorPassThreshold sensorModel.ApPassThresholdHistory) (*sensorModel.ApPassThresholdHistory, error) {
	db := s.db.Create(&sensorPassThreshold)
	return &sensorPassThreshold, db.Error
}

func (s sensorRepo) UpdateSensorData(sensorData sensorModel.ApSensor) (*sensorModel.ApSensor, error) {
	db := s.db.Save(&sensorData)
	return &sensorData, db.Error
}
