package sensorRepository

import "airqi-be/internal/app/model/sensorModel"

type ISensorRepository interface {
	GetAllSensor() (*[]sensorModel.ApSensor, error)
	GetAllSensorWhereIsEnable(isEnable bool) (*[]sensorModel.ApSensor, error)
	GetSensorByDeviceID(sensorId string) (*sensorModel.ApSensor, error)
	GetAllSensorRead() (*[]sensorModel.ApSensorRead, error)
	GetLastSensorReadBySensorUID(sensorUID string) (*sensorModel.ApSensorRead, error)
	InsertSensorRead(sensorReadData sensorModel.ApSensorRead) (*sensorModel.ApSensorRead, error)
	InsertSensorPassThresholdHistory(sensorPassThreshold sensorModel.ApPassThresholdHistory) (*sensorModel.ApPassThresholdHistory, error)
	GetSensorThresholdsBySensorID(sensorId int64) (*[]sensorModel.ApSensorThreshold, error)
	GetSensorThresholdBySensorIDAndReadValueBetweenTopThresholdValueAndBottomThresholdValueAndIsEnable(sensorId int64,
		readValue int64, isEnable bool) (*sensorModel.ApSensorThreshold, error)
	GetLastPassThresholdHistoryByThresholdID(thresholdId int64) (*sensorModel.ApPassThresholdHistory, error)
	GetLastPassThresholdHistoryBySensorID(sensorId int64) (*sensorModel.ApPassThresholdHistory, error)
	GetSensorReadsFromSensorIDAndReadValueBetweenAndOriginGMTCreatedBetween(sensorId int64, valueMin, valueMax, timeMin, timeMax int64) (*[]sensorModel.ApSensorRead, error)
	GetSensorReadsFromSensorIDAndReadValueBetweenAndOriginGMTCreatedBetweenSortBy(sensorId int64,
		valueMin, valueMax, timeMin, timeMax int64, sortBy string) (*[]sensorModel.ApSensorRead, error)
	GetSensorReadByReadUID(readUID string) (*sensorModel.ApSensorRead, error)
	UpdateSensorData(sensorData sensorModel.ApSensor) (*sensorModel.ApSensor, error)
}
