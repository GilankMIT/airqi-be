package userRepository

import "airqi-be/internal/app/model/userModel"

type IUserRepository interface {
	GetUserByEmail(email string) (*userModel.ApUser, error)
	GetUserByID(ID int64) (*userModel.ApUser, error)
	GetUserRoleByUserID(userID int64) (*[]userModel.ApRole, error)
}
