package userRepository

import (
	"airqi-be/internal/app/model/userModel"
	"gorm.io/gorm"
)

type userRepository struct {
	db *gorm.DB
}

func NewApUserRepository(db *gorm.DB) IUserRepository {
	return userRepository{db: db}
}

func (u userRepository) GetUserByEmail(email string) (*userModel.ApUser, error) {
	var apUserData userModel.ApUser
	db := u.db.First(&apUserData, "email = ?", email)
	return &apUserData, db.Error
}

func (u userRepository) GetUserRoleByUserID(userID int64) (*[]userModel.ApRole, error) {
	var apRoles []userModel.ApRole
	db := u.db.Find(&apRoles, "user_id = ?", userID)
	return &apRoles, db.Error
}

func (u userRepository) GetUserByID(ID int64) (*userModel.ApUser, error) {
	var apUser userModel.ApUser
	db := u.db.Find(&apUser, "id = ?", ID)
	return &apUser, db.Error
}
