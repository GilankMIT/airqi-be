package templateRenderer

import (
	"strings"
)

var (
	FilteredSymbol = []string{
		",", ".", "/", "(", ")", "*", "&", "^", "!", "@", "#", "$", "+", "-", "|", "[", "]", ":", ";", "?", "%",
	}
)

func Render(template string, data map[string]string) string {
	//retrieve template key
	templateKeys := make([]string, 0)

	words := strings.Split(template, " ")
	for _, word := range words {

		if len(word) < 2 {
			continue
		}

		if strings.Contains(word, "%") {
			templateKeys = append(templateKeys, word)
		}
	}

	for _, templateKey := range templateKeys {

		filteredTemplateKey := cleanWordFromSymbols(templateKey)

		actualData, ok := data[filteredTemplateKey]
		if !ok {
			actualData = "default"
		}

		template = strings.ReplaceAll(template, "%"+filteredTemplateKey, actualData)
	}

	return template
}

func cleanWordFromSymbols(word string) string {
	for _, symbol := range FilteredSymbol {
		word = strings.ReplaceAll(word, symbol, "")
	}

	return word
}
