package templateRenderer

import (
	"testing"
)

func TestRender(t *testing.T) {
	expectedValue := "Hello gilang, good morning! Hope you are fine, current project progress is 25% [testing]"

	newData := Render("Hello %name, good %greet! Hope you are %condition, current project progress is %percentage% [%template_name]",
		map[string]string{
			"name":          "gilang",
			"greet":         "morning",
			"condition":     "fine",
			"percentage":    "25",
			"template_name": "testing"})
	if newData != expectedValue {
		t.Error("template does not match", "expecting : ", expectedValue, " got : ", newData)
	}
}
