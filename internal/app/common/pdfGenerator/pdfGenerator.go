package pdfGenerator

import (
	"bytes"
	"github.com/SebastiaanKlippert/go-wkhtmltopdf"
	"github.com/rs/zerolog/log"
	"html/template"
	"io/ioutil"
	"os"
	"strconv"
)

//pdf requestpdf struct
type RequestPdf struct {
	body string
}

//new request to pdf function
func NewRequestPdf(body string) *RequestPdf {
	return &RequestPdf{
		body: body,
	}
}

//parsing template function
func (r *RequestPdf) ParseTemplate(templateFileName string, data interface{}) error {

	t, err := template.ParseFiles(templateFileName)
	if err != nil {
		return err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return err
	}
	r.body = buf.String()
	return nil
}

//generate pdf function
func (r *RequestPdf) GeneratePDF(htmlOutputPath, pdfOutputPath string, id int64) (bool, error) {

	// write whole the body
	err1 := ioutil.WriteFile(htmlOutputPath+strconv.FormatInt(id, 10)+".html", []byte(r.body), 0777)
	if err1 != nil {
		panic(err1)
	}

	f, err := os.Open(htmlOutputPath + strconv.FormatInt(id, 10) + ".html")
	if f != nil {
		defer f.Close()
	}
	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	pdfg, err := wkhtmltopdf.NewPDFGenerator()
	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	pdfg.AddPage(wkhtmltopdf.NewPageReader(f))

	pdfg.PageSize.Set(wkhtmltopdf.PageSizeA4)

	pdfg.Dpi.Set(300)

	pdfg.TOC.EnableLocalFileAccess.Set(true)
	pdfg.TOC.EnableTocBackLinks.Set(true)

	err = pdfg.Create()
	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	err = pdfg.WriteFile(pdfOutputPath)
	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	return true, nil
}
