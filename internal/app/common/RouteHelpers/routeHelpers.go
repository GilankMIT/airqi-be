package RouteHelpers

import (
	"airqi-be/internal/app/common/jwtHelper"
	"airqi-be/internal/app/model/userModel"
	"errors"
	"github.com/gin-gonic/gin"
)

var (
	ErrorContextNotExist     = errors.New("user context not exist")
	ErrorParsingUserModel    = errors.New("error parsing user model")
	ErrInvalidJWTForProspect = errors.New("invalid jwt, forbidden access")
)

func GetUserFromJWTContext(c *gin.Context) (*userModel.User, error) {
	user, exists := c.Get("user")
	if !exists {
		return nil, ErrorContextNotExist
	}

	if user == "" {
		return nil, ErrorContextNotExist
	}

	userData, ok := user.(*userModel.User)
	if !ok {
		return nil, ErrorParsingUserModel
	}

	return userData, nil
}

func GetApplicantJWTContext(c *gin.Context) (*jwtHelper.APUserClaim, error) {
	applicantClaims, exists := c.Get("applicant")
	if !exists {
		return nil, ErrorContextNotExist
	}

	if applicantClaims == "" {
		return nil, ErrorContextNotExist
	}

	userData, ok := applicantClaims.(jwtHelper.APUserClaim)
	if !ok {
		return nil, ErrorParsingUserModel
	}

	return &userData, nil
}

func ValidateProspectJWTAgainstNIKAndPhoneReq(nik, phone string, c *gin.Context) error {
	prospectData, err := GetApplicantJWTContext(c)
	if err != nil {
		return err
	}

	if prospectData.NIK != nik {
		return ErrInvalidJWTForProspect
	}

	return nil
}

func ValidateProspectJWTAgainstNIKReq(nik string, c *gin.Context) error {
	prospectData, err := GetApplicantJWTContext(c)
	if err != nil {
		return err
	}

	if prospectData.NIK != nik {
		return ErrInvalidJWTForProspect
	}

	return nil
}
