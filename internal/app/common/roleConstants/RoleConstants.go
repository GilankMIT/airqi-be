package roleConstants

const (
	AdministratorRole   = "administrator"
	HRRole              = "human-resource"
	ILabRole            = "ilab"
	GeneralCustomerRole = "general-customer"
	AnalystRole         = "analyst"
	LoanAdminRole       = "loan-admin"
)

var UserRoleID = map[string]int{
	AdministratorRole:   1,
	HRRole:              2,
	GeneralCustomerRole: 3,
	ILabRole:            4,
	AnalystRole:         5,
	LoanAdminRole:       6,
}

var RoleName = map[int]string{
	UserRoleID[AdministratorRole]:   AdministratorRole,
	UserRoleID[HRRole]:              HRRole,
	UserRoleID[GeneralCustomerRole]: GeneralCustomerRole,
	UserRoleID[ILabRole]:            ILabRole,
	UserRoleID[AnalystRole]:         AnalystRole,
	UserRoleID[LoanAdminRole]:       LoanAdminRole,
}

//AllRole return string for all role route compatible
func AllRole() string {
	allRole := ""
	for _, userRole := range RoleName {
		if allRole != "" {
			allRole += "|"
		}
		allRole += userRole
	}

	return allRole
}
