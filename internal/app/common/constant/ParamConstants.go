package constant

const (
	AirqiDingMandatoryKeywordKey = "AIRQI_DING_MANDATORY_KEYWORD"
	AverageTimeStep              = int64(60)
)
