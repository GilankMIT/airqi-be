package constant

import "time"

const (
	SensorConstant_GlobAccValueMin              = 1
	SensorConstant_GlobAccValueMax              = 1500
	SensorConstant_GlobTimeRangeMin             = -7 * time.Hour * 24 //7 days
	SensorConstant_InsertReadValueSucess        = "SR_R_001"
	SensorConstant_InsertReadValuePendingSucess = "SR_R_002" //used for partial data packet sent
	SensorConstant_InsertReadValueFailed        = "SR_R_009"
)
