package constant

const (
	NotificationAccountsDing  = "DING_NOTIF_ACC"
	NotificationAccountsEmail = "DING_NOTIF_ACC"

	NotificationStatusPROCESSING int8 = 1
	NotificationStatusSUCCESS    int8 = 2
	NotificationStatusFAILED     int8 = 3
)
