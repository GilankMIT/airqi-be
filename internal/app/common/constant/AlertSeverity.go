package constant

const (
	ThresholdSeverityDefault   = 0
	ThresholdSeverityPoor      = 4
	ThresholdSeverityExcellent = 1
	ThresholdSeverityWarning   = 2
	ThresholdSeverityDanger    = 3
	ThresholdSeveritySafe      = 8
)

var ThresholdLevelMap = map[int]string{
	ThresholdSeveritySafe:      "GOOD",
	ThresholdSeverityWarning:   "WARNING",
	ThresholdSeverityDanger:    "DANGER",
	ThresholdSeverityDefault:   "DEFAULT",
	ThresholdSeverityExcellent: "EXCELLENT",
	ThresholdSeverityPoor:      "POOR",
}
