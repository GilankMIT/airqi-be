package commons

import (
	"airqi-be/internal/app/appcontext"
	"github.com/go-playground/validator/v10"
	"github.com/streadway/amqp"
	"gorm.io/gorm"
)

// Options common option for all object that needed
type Options struct {
	AppCtx       *appcontext.AppContext
	Db           *gorm.DB
	DbSikp       *gorm.DB
	UUID         Iuuid
	Validator    *validator.Validate
	MsgQueueConn *amqp.Connection
	MsgQueueChan *amqp.Channel
}
