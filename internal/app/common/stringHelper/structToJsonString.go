package stringHelper

import (
	"encoding/json"
	"strings"

	"github.com/spf13/cast"
)

func ToJSONString(req interface{}) string {
	res, _ := json.Marshal(req)
	return string(res)
}

func ConvertBVTraceNoToInt(traceNo string) int {
	result := ""
	y := false
	for i, s := range strings.Split(traceNo, "") {
		if i == 0 {
			if s == "-" {
				result += s
			}
		} else {
			if s != "0" {
				y = true
			}
			if y {
				result += s
			}
		}
	}

	return cast.ToInt(result)
}
