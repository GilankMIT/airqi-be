package binaryFile

import (
	"bytes"
	"net/http"
	"time"
)

func ExportTxt(data string, w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Disposition", "attachment; filename="+"AskrindoReq.txt")
	w.Header().Set("Content-Transfer-Encoding", "binary")
	w.Header().Set("Expires", "0")
	http.ServeContent(w, r, "AskrindoReq.txt", time.Now(), bytes.NewReader([]byte(data)))
}
