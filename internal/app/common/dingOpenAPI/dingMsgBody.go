package dingOpenAPI

const DingMsgTypeTEXT = "text"

type DingBodyMsgText struct {
	At struct {
		AtMobiles []string `json:"atMobiles"`
		AtUserIds []string `json:"atUserIds"`
		IsAtAll   bool     `json:"isAtAll"`
	} `json:"at"`

	Text struct {
		Content string `json:"content"`
	} `json:"text"`

	MsgType string `json:"msgtype"`
}
