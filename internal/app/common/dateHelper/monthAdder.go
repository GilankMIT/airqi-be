package dateHelper

import (
	"errors"
	"time"

	"github.com/rs/zerolog/log"
)

func GetNextMonth(currentDate time.Time) time.Time {
	currentYear, currentMonth, currentDay := currentDate.Date()
	currentLocation := currentDate.Location()

	firstOfMonth := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, currentLocation)
	lastOfMonth := firstOfMonth.AddDate(0, 1, -1)

	//get date difference
	nextDate := currentDate.AddDate(0, 0, (lastOfMonth.Day()-currentDay)+currentDay)

	return nextDate
}

func GetDateFromMonthTenor(monthTenor int, startDate time.Time) time.Time {
	if monthTenor == 0 {
		return startDate
	}

	dueTime := startDate.AddDate(0, monthTenor, 0)
	return dueTime
}

func GetCurrentDateRangeFromStringDate(dateString string) (intTime int64, err error) {
	if len(dateString) != 8 {
		return 0, errors.New("invalid date format, supported date format is YYYYMMDD")
	}
	currentTime := time.Now()
	parsedDate, err := time.Parse("2006-01-02", dateString[:4]+"-"+dateString[4:6]+"-"+dateString[6:])
	if err != nil {
		log.Error().Msg(err.Error())
		return 0, err
	}

	//get time (unix) range
	timeRange := parsedDate.Unix() - currentTime.Unix()
	return timeRange, nil
}
