package objectDumper

import "fmt"

func DumpStruct(data interface{}) string {
	return fmt.Sprintf("%+v ", data)
}
