package functionLogging

import (
	"airqi-be/internal/app/model/helperModel"
	"encoding/json"
	"fmt"
	"runtime"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cast"
)

func LogError(subRoutineName, errorMessage string, err error) {
	log.Error().Msg(subRoutineName + " " + errorMessage + " " + err.Error())
}

func LogErrorV2(url, request, response, message string) {
	errorBody := helperModel.ErrorMessage{
		Url:      url,
		Request:  request,
		Response: response,
		Message:  message,
	}
	a, _ := json.Marshal(errorBody)
	log.Error().Msg(cast.ToString(a))
}
func LogInfoV2(url, request, response, message string) {
	errorBody := helperModel.ErrorMessage{
		Url:      url,
		Request:  request,
		Response: response,
		Message:  message,
	}
	a, _ := json.Marshal(errorBody)
	log.Info().Msg(cast.ToString(a))
}

func Log(subroutine, message string, errSys error) {
	colorReset := "\033[0m"

	colorRed := "\033[31m"
	colorGreen := "\033[32m"
	colorYellow := "\033[33m"

	filePath, lineNumber := getFilePathAndLineNumber()
	funcName := getFuncName()
	fmt.Println(colorRed + "=================<ERROR>=================")
	fmt.Println(colorGreen+"file :"+colorReset, filePath)
	fmt.Println(colorGreen+"func :"+colorReset, funcName)
	fmt.Println(colorGreen+"line :"+colorReset, lineNumber)
	fmt.Println(colorGreen+"subroutine :"+colorGreen, subroutine)
	fmt.Println(colorGreen+"dev-message :"+colorYellow, message)
	fmt.Println(colorGreen+"err-sys :"+colorRed, errSys.Error())
	fmt.Println(colorRed + "=================</ERROR>=================" + colorReset)
}

func getFuncName() string {
	pc := make([]uintptr, 1)
	runtime.Callers(3, pc)
	f := runtime.FuncForPC(pc[0])
	return f.Name()
}

func getFilePathAndLineNumber() (string, string) {
	_, filePath, lineNumber, ok := runtime.Caller(2)
	if !ok {
		return "<unknowed file name>", "<unknowed line number>"
	}
	return filePath, cast.ToString(lineNumber)
}
