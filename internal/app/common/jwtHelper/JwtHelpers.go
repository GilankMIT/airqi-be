package jwtHelper

import (
	applicationConstants2 "airqi-be/internal/app/common/applicationConstants"
	"errors"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
)

var (
	ErrTokenExpired = errors.New("token already expired")
)

var (
	//jwtSignatureKey is a secret key to hash the JWT Token
	jwtSignatureKey string
)

//APUserClaim - represent object of prospect clains
type APUserClaim struct {
	jwt.StandardClaims
	APUserID  int64  `json:"ap_user_id"`
	APUserUID string `json:"ap_user_uid"`
	ExpiresAt int64  `json:"exp,omitempty"`
}

func init() {
	jwtSignatureKey = os.Getenv(applicationConstants2.JWT_SECRET_KEY)
}

//NewWithClaims will return token with custom claims
func NewWithClaims(claims jwt.Claims) (string, error) {

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString([]byte(jwtSignatureKey))

	if err != nil {
		return "", err
	}
	return ss, nil
}

//VerifyTokenWithAPUserClaim will verify the validity of token and return the claims
func VerifyTokenWithAPUserClaim(token string) (*APUserClaim, error) {

	jwtToken, err := jwt.ParseWithClaims(token, &APUserClaim{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(jwtSignatureKey), nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := jwtToken.Claims.(*APUserClaim)
	if !ok {
		return nil, errors.New("error retrieving claims")
	}

	timeNow := time.Now().Unix()
	if claims.ExpiresAt < timeNow {
		return nil, ErrTokenExpired
	}

	return claims, nil
}
