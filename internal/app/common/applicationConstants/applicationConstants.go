package applicationConstants

const (
	MigrateUp = iota
	MigrateDown
)

const (
	//env field parameter
	JWT_SECRET_KEY              = "JWT_SECRET_KEY"
	JWT_EXPIRATION_DURATION_DAY = "JWT_EXPIRATION_DURATION_DAY"
)
