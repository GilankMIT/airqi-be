package sensorHelper

import "airqi-be/internal/app/model/sensorModel"

func GetAverageFromSensorRead(sensorReads []sensorModel.ApSensorRead) int64 {
	sum := int64(0)
	for _, read := range sensorReads {
		sum += read.ReadValue
	}

	//prevent divide by zero
	if sum == 0 {
		return 0
	}

	return sum / int64(len(sensorReads))
}
