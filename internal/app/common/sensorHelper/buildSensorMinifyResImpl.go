package sensorHelper

import "airqi-be/internal/app/model/sensorModel"

func BuildSensorMinifyResImpl(sensorReadData *[]sensorModel.ApSensorRead) (sensorReadDataMinify []sensorModel.
	SensorReadMinifyRes) {
	if sensorReadData == nil {
		return sensorReadDataMinify
	}

	for _, readData := range *sensorReadData {
		sensorReadDataMinify = append(sensorReadDataMinify, sensorModel.SensorReadMinifyRes{
			ApSensorID:       readData.ApDeviceID,
			ReadValue:        readData.ReadValue,
			ReadDesc:         readData.ReadDesc,
			ReadUID:          readData.ReadUID,
			OriginGMTCreated: readData.OriginGMTCreated,
		})
	}

	return sensorReadDataMinify
}
