package timeFunc

import "time"

func CurrentTime() int64 {
	return time.Now().UnixNano() / 1000000
}
