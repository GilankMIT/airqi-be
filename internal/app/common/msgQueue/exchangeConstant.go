package msgQueue

type Exchange struct {
	Name string
	Type string
}

const (
	ExchangeNameSensorReadCommon = "EXCHG_EVCOMM001"
	ExchangeNameSensorInfoCommon = "EXCHG_EVCOMM002"
	ExchangeNameAlertInfoCommon  = "EXCHG_EVCOMM003"
)

var (
	ExchangeSensorReadCommon = Exchange{
		Name: ExchangeNameSensorReadCommon,
		Type: "fanout",
	}

	ExchangeSensorInfoCommon = Exchange{
		Name: ExchangeNameSensorInfoCommon,
		Type: "fanout",
	}

	ExchangeAlertInfoCommon = Exchange{
		Name: ExchangeNameAlertInfoCommon,
		Type: "fanout",
	}
)

var Exchanges = []Exchange{
	ExchangeSensorReadCommon,
	ExchangeSensorInfoCommon,
	ExchangeAlertInfoCommon,
}
