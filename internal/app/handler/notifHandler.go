package handler

import (
	"airqi-be/internal/app/common/jsonHttpResponse"
	"github.com/gin-gonic/gin"
)

type NotifHandler struct {
	HandlerOption
}

func (handler NotifHandler) GetAllNotifAlert(c *gin.Context) {
	res, err := handler.NotifService.GetAllNotifAlerts()
	if err != nil {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      err.Error(),
		}
		jsonHttpResponse.InternalServerError(c, errPayload)
		return
	}

	successResponse := jsonHttpResponse.SuccessResponse{
		Status:       jsonHttpResponse.SuccessStatus,
		ResponseCode: "00",
		Message:      "displayed all sensors",
		Data:         res,
	}
	jsonHttpResponse.OK(c, successResponse)
}
