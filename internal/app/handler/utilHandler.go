package handler

import (
	"airqi-be/internal/app/common/jsonHttpResponse"
	"github.com/gin-gonic/gin"
)

type utilHandler struct{}

func NewUtilDelivery() utilHandler {
	return utilHandler{}
}

func (utilDelivery utilHandler) HealthyCheck(c *gin.Context) {
	successPayload := jsonHttpResponse.SuccessResponse{
		Status:       jsonHttpResponse.SuccessStatus,
		ResponseCode: "00",
		Message:      "Healthy check",
		Data:         nil,
	}
	jsonHttpResponse.OK(c, successPayload)
	return
}
