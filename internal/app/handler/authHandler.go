package handler

import (
	"airqi-be/internal/app/common/jsonHttpResponse"
	"airqi-be/internal/app/common/requestvalidationerror"
	"airqi-be/internal/app/model/authModel"
	"airqi-be/internal/app/service/authService"
	"github.com/gin-gonic/gin"
)

type AuthHandler struct {
	HandlerOption
}

func (authDelivery AuthHandler) Login(c *gin.Context) {
	var request authModel.ReqAuthenticate
	errBind := c.ShouldBind(&request)
	if errBind != nil {
		validations := requestvalidationerror.GetvalidationError(errBind)

		if len(validations) > 0 {
			jsonHttpResponse.NewFailedMissingRequiredFieldResponse(c, validations)
			return
		}

		jsonHttpResponse.NewFailedBadRequestResponse(c, errBind.Error())
		return
	}

	loginRes, err := authDelivery.AuthService.AuthenticateUser(request)
	if err != nil {
		if err == authService.ErrInvalidCredential {
			errPayload := jsonHttpResponse.FailedResponse{
				Status:       jsonHttpResponse.FailedStatus,
				ResponseCode: "00",
				Message:      err.Error(),
			}
			jsonHttpResponse.Unauthorized(c, errPayload)
			return
		}
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      err.Error(),
		}
		jsonHttpResponse.InternalServerError(c, errPayload)
		return
	}

	successPayload := jsonHttpResponse.SuccessResponse{
		Status:       jsonHttpResponse.SuccessStatus,
		ResponseCode: "00",
		Message:      "",
		Data:         loginRes,
	}
	jsonHttpResponse.OK(c, successPayload)
	return
}
