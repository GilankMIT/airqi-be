package handler

import (
	"airqi-be/internal/app/common/jsonHttpResponse"
	"airqi-be/internal/app/common/requestvalidationerror"
	"airqi-be/internal/app/model/sensorModel"
	"github.com/gin-gonic/gin"
	"strconv"
)

type SensorHandler struct {
	HandlerOption
}

func (sensorHandler *SensorHandler) GetAllSensor(c *gin.Context) {
	sensorData, err := sensorHandler.HandlerOption.SensorService.GetAllSensors()
	if err != nil {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      err.Error(),
		}
		jsonHttpResponse.InternalServerError(c, errPayload)
		return
	}

	successResponse := jsonHttpResponse.SuccessResponse{
		Status:       jsonHttpResponse.SuccessStatus,
		ResponseCode: "00",
		Message:      "displayed all sensors",
		Data:         sensorData,
	}
	jsonHttpResponse.OK(c, successResponse)

}

func (sensorHandler *SensorHandler) GetSensorReadBySensorID(c *gin.Context) {
	sensorIdParam := c.Query("sensor_id")
	if sensorIdParam == "" {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      "please specify sensor id",
		}

		jsonHttpResponse.BadRequest(c, errPayload)
		return
	}

	timeMinParam := c.Query("time_min")
	timeMin, err := strconv.ParseInt(timeMinParam, 10, 64)
	if err != nil {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      "cannot parse time min " + err.Error(),
		}

		jsonHttpResponse.BadRequest(c, errPayload)
		return
	}

	timeMaxParam := c.Query("time_max")
	timeMax, err := strconv.ParseInt(timeMaxParam, 10, 64)
	if err != nil {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      "cannot parse time max " + err.Error(),
		}

		jsonHttpResponse.BadRequest(c, errPayload)
		return
	}

	req := sensorModel.ReqSensorReadInfo{
		ApDeviceID: sensorIdParam,
		TimeMin:    timeMin,
		TimeMax:    timeMax,
	}

	sensorReadInfo, err := sensorHandler.HandlerOption.SensorService.GetSensorReadInfo(req)
	if err != nil {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      err.Error(),
		}
		jsonHttpResponse.InternalServerError(c, errPayload)
		return
	}

	successResponse := jsonHttpResponse.SuccessResponse{
		Status:       jsonHttpResponse.SuccessStatus,
		ResponseCode: "00",
		Message:      "displayed sensor reads for sensor ID " + sensorIdParam,
		Data:         sensorReadInfo,
	}
	jsonHttpResponse.OK(c, successResponse)
}

func (sensorHandler *SensorHandler) GetAverageSensorReadsBySensorID(c *gin.Context) {
	sensorIdParam := c.Query("sensor_id")
	if sensorIdParam == "" {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      "please specify sensor id",
		}

		jsonHttpResponse.BadRequest(c, errPayload)
		return
	}

	timeMinParam := c.Query("time_min")
	timeMin, err := strconv.ParseInt(timeMinParam, 10, 64)
	if err != nil {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      "cannot parse time min " + err.Error(),
		}

		jsonHttpResponse.BadRequest(c, errPayload)
		return
	}

	timeMaxParam := c.Query("time_max")
	timeMax, err := strconv.ParseInt(timeMaxParam, 10, 64)
	if err != nil {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      "cannot parse time max " + err.Error(),
		}

		jsonHttpResponse.BadRequest(c, errPayload)
		return
	}

	req := sensorModel.ReqSensorReadInfo{
		ApDeviceID: sensorIdParam,
		TimeMin:    timeMin,
		TimeMax:    timeMax,
	}

	sensorReadInfo, err := sensorHandler.HandlerOption.SensorService.GetAverageSensorReadsInfo(req)
	if err != nil {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      err.Error(),
		}
		jsonHttpResponse.InternalServerError(c, errPayload)
		return
	}

	successResponse := jsonHttpResponse.SuccessResponse{
		Status:       jsonHttpResponse.SuccessStatus,
		ResponseCode: "00",
		Message:      "displayed sensor reads for sensor ID " + sensorIdParam,
		Data:         sensorReadInfo,
	}
	jsonHttpResponse.OK(c, successResponse)
}

func (sensorHandler *SensorHandler) GetAverageSensorReadBySensorID(c *gin.Context) {
	sensorIdParam := c.Query("sensor_id")
	if sensorIdParam == "" {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      "please specify sensor id",
		}

		jsonHttpResponse.BadRequest(c, errPayload)
		return
	}

	timeScaleParam := c.Query("time_scale")
	timeScale, err := strconv.ParseInt(timeScaleParam, 10, 64)
	if err != nil {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      "cannot parse time scale " + err.Error(),
		}

		jsonHttpResponse.BadRequest(c, errPayload)
		return
	}

	sensorReadInfo, err := sensorHandler.HandlerOption.SensorService.GetSensorReadAverageBySensorUID(sensorIdParam, timeScale)
	if err != nil {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      err.Error(),
		}
		jsonHttpResponse.InternalServerError(c, errPayload)
		return
	}

	successResponse := jsonHttpResponse.SuccessResponse{
		Status:       jsonHttpResponse.SuccessStatus,
		ResponseCode: "00",
		Message:      "displayed sensor reads for sensor ID " + sensorIdParam,
		Data:         sensorReadInfo,
	}
	jsonHttpResponse.OK(c, successResponse)
}

func (sensorHandler *SensorHandler) InsertSensorRead(c *gin.Context) {
	var request sensorModel.SensorInsertReq
	errBind := c.ShouldBind(&request)
	if errBind != nil {
		validations := requestvalidationerror.GetvalidationError(errBind)

		if len(validations) > 0 {
			jsonHttpResponse.NewFailedMissingRequiredFieldResponse(c, validations)
			return
		}

		jsonHttpResponse.NewFailedBadRequestResponse(c, errBind.Error())
		return
	}

	sensorData, err := sensorHandler.HandlerOption.SensorService.AddSensorRead(request)
	if err != nil {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      err.Error(),
		}
		jsonHttpResponse.InternalServerError(c, errPayload)
		return
	}

	successResponse := jsonHttpResponse.SuccessResponse{
		Status:       jsonHttpResponse.SuccessStatus,
		ResponseCode: "00",
		Message:      "sensor read inserted successfully",
		Data:         sensorData,
	}
	jsonHttpResponse.OK(c, successResponse)
}

func (sensorHandler *SensorHandler) UpdateSensorInfo(c *gin.Context) {
	var request sensorModel.ReqUpdateSensorInfo
	errBind := c.ShouldBind(&request)
	if errBind != nil {
		validations := requestvalidationerror.GetvalidationError(errBind)

		if len(validations) > 0 {
			jsonHttpResponse.NewFailedMissingRequiredFieldResponse(c, validations)
			return
		}

		jsonHttpResponse.NewFailedBadRequestResponse(c, errBind.Error())
		return
	}

	sensorData, err := sensorHandler.HandlerOption.SensorService.UpdateSensorData(request)
	if err != nil {
		errPayload := jsonHttpResponse.FailedResponse{
			Status:       jsonHttpResponse.FailedStatus,
			ResponseCode: "00",
			Message:      err.Error(),
		}
		jsonHttpResponse.InternalServerError(c, errPayload)
		return
	}

	successResponse := jsonHttpResponse.SuccessResponse{
		Status:       jsonHttpResponse.SuccessStatus,
		ResponseCode: "00",
		Message:      "sensor updated successfully",
		Data:         sensorData,
	}
	jsonHttpResponse.OK(c, successResponse)
}

func (SensorHandler SensorHandler) InitSensorListeners() error {
	return SensorHandler.SensorAlertListenerService.InitListening()
}
