package handler

import (
	commons "airqi-be/internal/app/common"
	"airqi-be/internal/app/service"
	"airqi-be/internal/app/websocket"
)

// HandlerOption option for handler, including all service
type HandlerOption struct {
	commons.Options
	*service.Services
	WSHandler *websocket.WebsocketHandler
}
