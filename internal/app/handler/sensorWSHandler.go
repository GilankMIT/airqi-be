package handler

import (
	"airqi-be/internal/app/model/sensorModel"
	"airqi-be/internal/app/service/notifService"
	"airqi-be/internal/app/service/sensorService"
	websocketHandler "airqi-be/internal/app/websocket"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	uuid "github.com/satori/go.uuid"
	"net/http"
	"strings"
)

var (
	wsError_messageQueryError           = []byte(`{"error" : 1, "Message" : "incorrect Message query"}`)
	wsError_messageQueryCommandNotFound = []byte(`{"error" : 2, "Message" : "command not found"}`)
	wsError_InternalServerErrorRequest  = []byte(`{"error" : 500, "Message" : "internal server error"}`)

	newline = []byte{'\n'}
	space   = []byte{' '}
)

type sensorWSHandler struct {
	websocketHandler          *websocketHandler.WebsocketHandler
	wsUpgrader                websocket.Upgrader
	hub                       *Hub
	sensorReadListenerService sensorService.ISensorReadListenerService
	alertReadListenerService  notifService.INotifEventListenerService
}

type Client struct {
	Hub              *Hub
	Conn             *websocket.Conn
	Send             chan []byte
	ConnectionID     uuid.UUID
	userID           int
	SessionKey       string
	ShouldDisconnect bool
}

type Hub struct {
	clients    map[string]*Client
	register   chan *Client
	unregister chan *Client

	message chan *ClientMessage
}

type ClientMessage struct {
	Client  *Client
	Message []byte
}

func NewSensorWSHandler(websocketHandler *websocketHandler.WebsocketHandler,
	sensorReadListenerService sensorService.ISensorReadListenerService,
	alertReadListenerService notifService.INotifEventListenerService) {

	hub := newHub()

	handler := sensorWSHandler{
		websocketHandler:          websocketHandler,
		hub:                       hub,
		sensorReadListenerService: sensorReadListenerService,
		alertReadListenerService:  alertReadListenerService,
	}

	handler.wsUpgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
	go handler.runWebsocketHub()

	websocketRoute := handler.websocketHandler.GetWSRouter().Group("/api/ws")
	{
		websocketRoute.GET("sensor-read-info", handler.handleWSConnection)
	}
}

func (handler *sensorWSHandler) handleWSConnection(c *gin.Context) {
	handler.sensorWSConnectionHandler(c.Writer, c.Request)
}

func (handler *sensorWSHandler) sensorWSConnectionHandler(w http.ResponseWriter, r *http.Request) {
	handler.wsUpgrader.CheckOrigin = func(r *http.Request) bool { return true }
	conn, err := handler.wsUpgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Err(err)
		return
	}

	newClientUUID := uuid.NewV4()
	newClient := &Client{
		Hub:          handler.hub,
		Conn:         conn,
		Send:         make(chan []byte),
		ConnectionID: newClientUUID,
	}

	newClient.Hub.register <- newClient

	go newClient.readPumper()
	go newClient.writePumper()
}

func newHub() *Hub {
	return &Hub{
		clients:    make(map[string]*Client),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		message:    make(chan *ClientMessage),
	}
}

func (handler *sensorWSHandler) runWebsocketHub() {
	for {
		select {

		//New Client connect
		case client := <-handler.hub.register:
			handler.hub.clients[client.ConnectionID.String()] = client
			log.Info().Msg("[Session] new connection: " + client.ConnectionID.String())

		//Client disconnect
		case client := <-handler.hub.unregister:
			lineString := fmt.Sprintf("diconnected user %s", client.ConnectionID)
			log.Info().Msg(lineString)
			delete(handler.hub.clients, client.ConnectionID.String())

		//Client receive new Message
		case clientMessage := <-handler.hub.message:
			handler.wsQueryMessageHandler(clientMessage.Client, clientMessage.Message)
		}
	}
}

/*
	readPumper will handle the websocket Message from connection layer and pass the Message
	it is like the delivery of URL in HTTP Router, but it takes websocket Message instead
*/
func (c *Client) readPumper() {
	defer func() {
		c.Hub.unregister <- c
		c.Conn.Close()
	}()

	for {
		_, message, err := c.Conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}
		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))

		c.Hub.message <- &ClientMessage{
			Client:  c,
			Message: message,
		}
	}
}

func (c *Client) writePumper() {
	defer func() {
		c.Conn.Close()
	}()

	for {
		select {
		case message := <-c.Send:
			w, err := c.Conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)
			if err := w.Close(); err != nil {
				break
			}
		}
	}
}

func (handler *sensorWSHandler) wsQueryMessageHandler(c *Client, message []byte) {

	//Start Message Parser
	//!Important note : only accept JSON with no nested JSON
	wsQueryMessage := strings.Split(string(message), " {")

	fmt.Println(string(message))
	//Error, Message query length is empty
	if len(wsQueryMessage) < 1 {
		c.Send <- wsError_messageQueryError
		return
	}

	//Error, Message query length is less than 2
	if len(wsQueryMessage) < 2 {
		c.Send <- wsError_messageQueryError
		return
	}

	//First index of Message query is the websocket command / route
	wsCommand := wsQueryMessage[0]
	wsPayload := []byte("{" + wsQueryMessage[1]) //Add leading "{" as it is removed during split process

	switch wsCommand {
	case "SENSOR_READ_LISTEN":
		go handler.initSensorListening(c, wsPayload)
	case "ALERT_LISTEN":
		go handler.initAlertListening(c, wsPayload)
	}
}

func (handler *sensorWSHandler) initSensorListening(c *Client, payload []byte) {
	var wsReq sensorModel.ReqSensorReadWS
	err := json.Unmarshal(payload, &wsReq)
	if err != nil {
		log.Error().Msg(err.Error())
		return
	}

	//init listening
	err = handler.sensorReadListenerService.InitListening(wsReq.SensorID, c.Send)
	if err != nil {
		log.Error().Msg(err.Error())
	}
}

func (handler *sensorWSHandler) initAlertListening(c *Client, payload []byte) {
	var wsReq sensorModel.ReqSensorReadWS
	err := json.Unmarshal(payload, &wsReq)
	if err != nil {
		log.Error().Msg(err.Error())
		return
	}

	//init listening
	err = handler.alertReadListenerService.InitListening("*", c.Send)
	if err != nil {
		log.Error().Msg(err.Error())
	}
}
