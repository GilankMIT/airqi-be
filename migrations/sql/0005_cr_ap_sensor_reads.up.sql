create table if not exists ap_sensor_reads
(
    id bigint auto_increment
        primary key,
    ap_sensor_id bigint null,
    ap_device_id varchar(128) null,
    read_value bigint null,
    read_desc longtext null,
    extend_info longtext null,
    seq_no bigint null,
    read_uid longtext null,
    origin_gmt_created bigint null,
    created_by bigint null,
    updated_by bigint null,
    gmt_created bigint null,
    gmt_modified bigint null
);