create table if not exists ap_sensor_types
(
    id bigint auto_increment
        primary key,
    type longtext null,
    is_telemetry bigint null,
    gmt_created bigint null,
    gmt_modified bigint null,
    created_by bigint null,
    updated_by bigint null
);