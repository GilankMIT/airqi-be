create table if not exists ap_notif_accounts
(
    id bigint auto_increment
        primary key,
    notif_type varchar(24) null,
    token text null,
    is_token_valid boolean null,
    is_main_acc boolean null,
    is_enable boolean null,
    should_retry boolean null,
    max_retry bigint null,
    priority int null,
    url longtext null,
    method varchar(24) null,
    gmt_created bigint null,
    gmt_modified bigint null,
    created_by bigint null,
    updated_by bigint null
);