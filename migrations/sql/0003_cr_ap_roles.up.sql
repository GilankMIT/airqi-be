create table ap_roles
(
    id                      bigint auto_increment
        primary key,
    ap_user_id              bigint null,
    ap_action_permission_id bigint null,
    gmt_created             bigint null,
    gmt_modified            bigint null,
    created_by              bigint null,
    updated_by              bigint null
);