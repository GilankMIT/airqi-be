create table if not exists ap_notif_alerts
(
    id                  bigint auto_increment
        primary key,
    ap_alert_id         longtext null,
    content             longtext null,
    ap_notif_account_id bigint   null,
    title               longtext null,
    status              tinyint  null,
    severity            tinyint  null,
    sensor_id           bigint   null,
    threshold_id        bigint   null,
    extend_info         text     null,
    gmt_created         bigint   null,
    gmt_modified        bigint   null,
    created_by          bigint   null,
    updated_by          bigint   null
);