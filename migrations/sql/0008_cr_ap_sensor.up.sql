create table if not exists ap_sensors
(
    id bigint auto_increment
        primary key,
    ap_device_id longtext null,
    ap_sensor_type longtext null,
    sensor_name longtext null,
    sensor_ip longtext null,
    is_visible tinyint(1) null,
    is_enable tinyint(1) null,
    pass_threshold_timespan_ms bigint null,
    gmt_created bigint null,
    gmt_modified bigint null,
    created_by bigint null,
    updated_by bigint null
);