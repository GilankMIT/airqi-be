create table if not exists ap_sensor_thresholds
(
    id bigint auto_increment
        primary key,
    ap_sensor_id bigint null,
    top_threshold_value bigint null,
    bottom_threshold_value bigint null,
    pass_threshold_timespan_second bigint null,
    is_enable tinyint(1) null,
    send_notif tinyint(1) null,
    `desc` longtext null,
    created_by bigint null,
    updated_by bigint null,
    gmt_created bigint null,
    gmt_modified bigint null
);