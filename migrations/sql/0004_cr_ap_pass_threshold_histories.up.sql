create table if not exists ap_pass_threshold_histories
(
    id bigint auto_increment
        primary key,
    ap_sensor_id bigint null,
    ap_sensor_threshold_id bigint null,
    last_read_value bigint null,
    notif_sent tinyint(1) null,
    origin_gmt_created bigint null,
    created_by bigint null,
    updated_by bigint null,
    gmt_created bigint null,
    gmt_modified bigint null
);