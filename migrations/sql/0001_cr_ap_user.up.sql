create table ap_users
(
    id           bigint auto_increment
        primary key,
    uid          varchar(128) null,
    email        varchar(128) null,
    password     varchar(128) null,
    first_name   varchar(64)  null,
    last_name    varchar(64)  null,
    gmt_created  bigint       null,
    gmt_modified bigint       null,
    created_by   bigint       null,
    updated_by   bigint       null
);