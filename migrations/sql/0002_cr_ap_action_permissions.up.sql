create table ap_action_permissions
(
    id           bigint auto_increment
        primary key,
    uri          text       null,
    method       varchar(8) null,
    gmt_created  bigint     null,
    gmt_modified bigint     null,
    created_by   bigint     null,
    updated_by   bigint     null
);