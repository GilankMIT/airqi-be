package config

import (
	"time"

	"github.com/spf13/viper"
)

// Provider the config provider
type Provider interface {
	ConfigFileUsed() string
	Get(key string) interface{}
	GetBool(key string) bool
	GetDuration(key string) time.Duration
	GetFloat64(key string) float64
	GetInt(key string) int
	GetInt64(key string) int64
	GetSizeInBytes(key string) uint
	GetString(key string) string
	GetStringMap(key string) map[string]interface{}
	GetStringMapString(key string) map[string]string
	GetStringMapStringSlice(key string) map[string][]string
	GetStringSlice(key string) []string
	GetTime(key string) time.Time
	InConfig(key string) bool
	IsSet(key string) bool
	Unmarshal(rawVal interface{}, opts ...viper.DecoderConfigOption) error
}

var defaultConfig *viper.Viper

func init() {
	v := viper.New()
	// readTomlConfig(v)
	defaultConfig = v
}

// func readTomlConfig(v *viper.Viper) {
// 	// handle config path for unit test
// 	dirPath, err := os.Getwd()
// 	if err != nil {
// 		panic(fmt.Errorf("Error get working dir: %s", err))
// 	}
// 	dirPaths := strings.Split(dirPath, "/internal")

// 	v.AddConfigPath(fmt.Sprintf("%s/params", dirPaths[0]))
// 	v.AddConfigPath("./params")
// 	v.SetConfigName("config")

// 	err = v.ReadInConfig()
// 	if err != nil {
// 		panic(fmt.Sprint("Config file not found", err))
// 	}

// 	fmt.Println("Using config file:", v.ConfigFileUsed())
// }

// Config return provider so that you can read config anywhere
func Config() Provider {
	return defaultConfig
}
