package cmd

import (
	"airqi-be/config"
	"airqi-be/internal/app/appcontext"
	commons "airqi-be/internal/app/common"
	"airqi-be/internal/app/common/msgQueue"
	"airqi-be/internal/app/pluginProcessor/alertPluginProcessor"
	"airqi-be/internal/app/repository"
	"airqi-be/internal/app/repository/notifRepository"
	"airqi-be/internal/app/repository/sensorRepository"
	"airqi-be/internal/app/repository/userRepository"
	"airqi-be/internal/app/server"
	"airqi-be/internal/app/service"
	"airqi-be/internal/app/service/authService"
	"airqi-be/internal/app/service/notifService"
	"airqi-be/internal/app/service/sensorService"
	"errors"
	"fmt"
	"github.com/streadway/amqp"
	"os"

	"github.com/joho/godotenv"
	gologger "github.com/mo-taufiq/go-logger"
	"github.com/rs/zerolog/log"

	govalidator "github.com/go-playground/validator/v10"
	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
	"gorm.io/gorm"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "airqi-sensor",
	Short: "A brief description of your application",
	Long:  `A longer description that spans multiple lines and likely contains examples and usage of using your application.`,
	Run: func(cmd *cobra.Command, args []string) {
		loadEnv("")

		start()
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize()
}

func initCommonOptions() (options commons.Options, err error) {
	cfg := config.Config()
	app := appcontext.NewAppContext(cfg)

	logLevel := zerolog.InfoLevel
	logLevelP, err := zerolog.ParseLevel(os.Getenv("APP_LOG_LEVEL"))
	if err == nil {
		logLevel = logLevelP
	}
	zerolog.SetGlobalLevel(logLevel)

	goValidator := govalidator.New()

	var mysqlDB, mysqlDBSikp *gorm.DB
	if app.GetMysqlOption().IsEnable {
		mysqlDB, err = app.GetDBInstance(appcontext.DBDialectMysql)
		if err != nil {
			err = errors.New(fmt.Sprintf("Failed to start, error connect to DB MySQL | %v", err))
			return
		}
	}

	var msgBrokerConn *amqp.Connection
	mqUsername := os.Getenv("MQ_USERNAME")
	mqPassword := os.Getenv("MQ_PASSWORD")
	mqHost := os.Getenv("MQ_HOST")
	mqPort := os.Getenv("MQ_PORT")
	msgBrokerConn, err = amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", mqUsername, mqPassword, mqHost, mqPort))
	if err != nil {
		log.Error().Msg(err.Error())
		return
	}
	log.Info().Msg("success connecting to rabbitMQ server " + fmt.Sprintf("amqp://%s:%s/", mqHost, mqPort))

	ch, err := msgBrokerConn.Channel()
	if err != nil {
		log.Error().Msg(err.Error())
		return
	}

	for _, ex := range msgQueue.Exchanges {
		err = ch.ExchangeDeclare(
			ex.Name,
			ex.Type,
			true,
			false,
			false,
			false,
			nil)
		if err != nil {
			log.Error().Msg("failed declaring queue" + err.Error())
			return
		}
	}

	options = commons.Options{
		AppCtx:       app,
		Db:           mysqlDB,
		DbSikp:       mysqlDBSikp,
		UUID:         commons.NewUuid(),
		Validator:    goValidator,
		MsgQueueChan: ch,
		MsgQueueConn: msgBrokerConn,
	}

	return
}

func loadEnv(envName string) {
	gologger.LogConf.NestedLocationLevel = 2
	log.Logger = log.Output(
		zerolog.ConsoleWriter{
			Out:     os.Stderr,
			NoColor: false,
		},
	)

	dotenvPath := "params/.env"

	if envName == "test" {
		dotenvPath = "params/.env.test"
	}

	err := godotenv.Load(dotenvPath)
	if err != nil {
		log.Error().Msg("Error loading .env file")
	}
}

func start() {
	opt, err := initCommonOptions()
	if err != nil {
		log.Error().Msg(err.Error())
		return
	}

	repo := wiringRepository(repository.Option{
		Options: opt,
	})

	wiredServicePlugins := wiringServicePlugin(service.Option{
		Options:      opt,
		Repositories: repo,
	})

	wiredService := wiringService(service.Option{
		Options:      opt,
		Repositories: repo,
	}, *wiredServicePlugins)

	apServer := server.NewServer(opt, wiredService)

	defer opt.MsgQueueChan.Close()
	defer opt.MsgQueueConn.Close()

	// run app
	apServer.StartApp()
}

func wiringRepository(repoOption repository.Option) *repository.Repositories {
	repo := repository.Repositories{
		UserRepository:   userRepository.NewApUserRepository(repoOption.Db),
		SensorRepository: sensorRepository.NewSensorRepository(repoOption.Db),
		NotifRepository:  notifRepository.NewNotifRepo(repoOption.Db),
	}

	return &repo
}

func wiringServicePlugin(serviceOption service.Option) *service.ServiceProcessor {
	//ding alert sender
	dingAlertPlugin := alertPluginProcessor.NewDingAlert(serviceOption.SensorRepository)

	svcProc := service.ServiceProcessor{NotifServicePlugins: []notifService.INotifSendingProcessor{
		dingAlertPlugin,
	}}
	return &svcProc
}

func wiringService(serviceOption service.Option, serviceProcessor service.ServiceProcessor) *service.Services {
	bridgeSvc := service.Services{
		NotifService: notifService.NewNotifService(serviceOption.NotifRepository, serviceProcessor.NotifServicePlugins, serviceOption.MsgQueueChan),
	}

	svc := service.Services{
		AuthService:                authService.NewAuthService(serviceOption.UserRepository),
		SensorService:              sensorService.NewSensorService(serviceOption.SensorRepository, serviceOption.MsgQueueChan),
		NotifService:               notifService.NewNotifService(serviceOption.NotifRepository, serviceProcessor.NotifServicePlugins, serviceOption.MsgQueueChan),
		SensorAlertListenerService: sensorService.NewSensorActionService(serviceOption.SensorRepository, bridgeSvc.NotifService, serviceOption.MsgQueueChan),
		SensorReadListenerService:  sensorService.NewSensorReadListener(serviceOption.SensorRepository, serviceOption.MsgQueueChan),
		NotifAlertListenerService:  notifService.NewNotifEventListenerService(serviceOption.NotifRepository, serviceOption.SensorRepository, serviceOption.MsgQueueChan),
	}
	return &svc
}
