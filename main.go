package main

import (
	"airqi-be/cmd"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"time"
)

func main() {

	log.Logger = log.Output(
		zerolog.ConsoleWriter{
			Out:     os.Stderr,
			NoColor: false,
		},
	)

	// zlog.Logger = zlog.With().Caller().Logger()
	if err := setTimezone("Asia/Jakarta"); err != nil {
		log.Error().Msg(err.Error()) // most likely timezone not loaded in Docker OS
	}

	cmd.Execute()
}

func setTimezone(tz string) error {
	loc, err := time.LoadLocation(tz)
	if err != nil {
		return err
	}
	time.Local = loc
	return nil
}
